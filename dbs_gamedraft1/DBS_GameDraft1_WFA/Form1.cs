﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Media;

namespace DBS_GameDraft1_WFA
{
    public partial class Form1 : Form //Main Form1
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e) //Login Screen's PLAY Button
        {
            Character_Selection_Form f2 = new Character_Selection_Form(); //Creates new instance of the Player 1 Character Form
            f2.ShowDialog(); //Shows P1 Character Form
            this.Hide();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
          SoundPlayer audio = new SoundPlayer(DBS_GameDraft1_WFA.Properties.Resources.unbreakable_determination);
            audio.Play();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
