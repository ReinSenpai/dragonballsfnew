﻿namespace DBS_GameDraft1_WFA
{
    partial class Character_Selection_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Character_Selection_Form));
            this.label1 = new System.Windows.Forms.Label();
            this.p1readyBtn = new System.Windows.Forms.Button();
            this.GokuBtn = new System.Windows.Forms.Button();
            this.VegetaBtn = new System.Windows.Forms.Button();
            this.JirenBtn = new System.Windows.Forms.Button();
            this.BeerusBtn = new System.Windows.Forms.Button();
            this.VegitoBtn = new System.Windows.Forms.Button();
            this.WhisBtn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.Android17Btn = new System.Windows.Forms.Button();
            this.HitBtn = new System.Windows.Forms.Button();
            this.FriezaBtn = new System.Windows.Forms.Button();
            this.ZamasuBtn = new System.Windows.Forms.Button();
            this.GohanBtn = new System.Windows.Forms.Button();
            this.BlackBtn = new System.Windows.Forms.Button();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.TrunksBtn = new System.Windows.Forms.Button();
            this.SasukeBtn = new System.Windows.Forms.Button();
            this.ToppoBtn = new System.Windows.Forms.Button();
            this.MadaraBtn = new System.Windows.Forms.Button();
            this.KeflaBtn = new System.Windows.Forms.Button();
            this.NarutoBtn = new System.Windows.Forms.Button();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            this.pictureBox17 = new System.Windows.Forms.PictureBox();
            this.pictureBox18 = new System.Windows.Forms.PictureBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.pictureBox19 = new System.Windows.Forms.PictureBox();
            this.GrayBtn = new System.Windows.Forms.Button();
            this.SupermanBtn = new System.Windows.Forms.Button();
            this.NatsuBtn = new System.Windows.Forms.Button();
            this.ZenoBtn = new System.Windows.Forms.Button();
            this.JinBtn = new System.Windows.Forms.Button();
            this.SaitamaBtn = new System.Windows.Forms.Button();
            this.pictureBox20 = new System.Windows.Forms.PictureBox();
            this.pictureBox21 = new System.Windows.Forms.PictureBox();
            this.pictureBox22 = new System.Windows.Forms.PictureBox();
            this.pictureBox23 = new System.Windows.Forms.PictureBox();
            this.pictureBox24 = new System.Windows.Forms.PictureBox();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox24)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(304, 7);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(301, 25);
            this.label1.TabIndex = 8;
            this.label1.Text = "Player One Choose Your Fighter!";
            // 
            // p1readyBtn
            // 
            this.p1readyBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.p1readyBtn.Location = new System.Drawing.Point(367, 607);
            this.p1readyBtn.Margin = new System.Windows.Forms.Padding(2);
            this.p1readyBtn.Name = "p1readyBtn";
            this.p1readyBtn.Size = new System.Drawing.Size(172, 41);
            this.p1readyBtn.TabIndex = 15;
            this.p1readyBtn.Text = "READY!";
            this.p1readyBtn.UseVisualStyleBackColor = true;
            this.p1readyBtn.Click += new System.EventHandler(this.p1readyBtn_Click);
            // 
            // GokuBtn
            // 
            this.GokuBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GokuBtn.Location = new System.Drawing.Point(76, 214);
            this.GokuBtn.Margin = new System.Windows.Forms.Padding(2);
            this.GokuBtn.Name = "GokuBtn";
            this.GokuBtn.Size = new System.Drawing.Size(106, 46);
            this.GokuBtn.TabIndex = 17;
            this.GokuBtn.Text = "Goku";
            this.GokuBtn.UseVisualStyleBackColor = true;
            this.GokuBtn.Click += new System.EventHandler(this.SelectCharacter);
            // 
            // VegetaBtn
            // 
            this.VegetaBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.VegetaBtn.Location = new System.Drawing.Point(665, 214);
            this.VegetaBtn.Margin = new System.Windows.Forms.Padding(2);
            this.VegetaBtn.Name = "VegetaBtn";
            this.VegetaBtn.Size = new System.Drawing.Size(106, 46);
            this.VegetaBtn.TabIndex = 18;
            this.VegetaBtn.Text = "Vegeta";
            this.VegetaBtn.UseVisualStyleBackColor = true;
            this.VegetaBtn.Click += new System.EventHandler(this.SelectCharacter);
            // 
            // JirenBtn
            // 
            this.JirenBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JirenBtn.Location = new System.Drawing.Point(374, 489);
            this.JirenBtn.Margin = new System.Windows.Forms.Padding(2);
            this.JirenBtn.Name = "JirenBtn";
            this.JirenBtn.Size = new System.Drawing.Size(106, 46);
            this.JirenBtn.TabIndex = 19;
            this.JirenBtn.Text = "Jiren";
            this.JirenBtn.UseVisualStyleBackColor = true;
            this.JirenBtn.Click += new System.EventHandler(this.SelectCharacter);
            // 
            // BeerusBtn
            // 
            this.BeerusBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BeerusBtn.Location = new System.Drawing.Point(76, 489);
            this.BeerusBtn.Margin = new System.Windows.Forms.Padding(2);
            this.BeerusBtn.Name = "BeerusBtn";
            this.BeerusBtn.Size = new System.Drawing.Size(106, 46);
            this.BeerusBtn.TabIndex = 20;
            this.BeerusBtn.Text = "Beerus";
            this.BeerusBtn.UseVisualStyleBackColor = true;
            this.BeerusBtn.Click += new System.EventHandler(this.SelectCharacter);
            // 
            // VegitoBtn
            // 
            this.VegitoBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.VegitoBtn.Location = new System.Drawing.Point(374, 214);
            this.VegitoBtn.Margin = new System.Windows.Forms.Padding(2);
            this.VegitoBtn.Name = "VegitoBtn";
            this.VegitoBtn.Size = new System.Drawing.Size(106, 46);
            this.VegitoBtn.TabIndex = 21;
            this.VegitoBtn.Text = "Vegito";
            this.VegitoBtn.UseVisualStyleBackColor = true;
            this.VegitoBtn.Click += new System.EventHandler(this.SelectCharacter);
            // 
            // WhisBtn
            // 
            this.WhisBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.WhisBtn.Location = new System.Drawing.Point(665, 489);
            this.WhisBtn.Margin = new System.Windows.Forms.Padding(2);
            this.WhisBtn.Name = "WhisBtn";
            this.WhisBtn.Size = new System.Drawing.Size(106, 46);
            this.WhisBtn.TabIndex = 22;
            this.WhisBtn.Text = "Whis";
            this.WhisBtn.UseVisualStyleBackColor = true;
            this.WhisBtn.Click += new System.EventHandler(this.SelectCharacter);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(754, 613);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 25);
            this.label2.TabIndex = 24;
            this.label2.Text = "Random";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Location = new System.Drawing.Point(12, 35);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(878, 567);
            this.tabControl1.TabIndex = 25;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.tabPage1.Controls.Add(this.pictureBox1);
            this.tabPage1.Controls.Add(this.BeerusBtn);
            this.tabPage1.Controls.Add(this.WhisBtn);
            this.tabPage1.Controls.Add(this.pictureBox4);
            this.tabPage1.Controls.Add(this.VegitoBtn);
            this.tabPage1.Controls.Add(this.pictureBox2);
            this.tabPage1.Controls.Add(this.JirenBtn);
            this.tabPage1.Controls.Add(this.GokuBtn);
            this.tabPage1.Controls.Add(this.VegetaBtn);
            this.tabPage1.Controls.Add(this.pictureBox6);
            this.tabPage1.Controls.Add(this.pictureBox5);
            this.tabPage1.Controls.Add(this.pictureBox3);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(870, 541);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Page 1";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::DBS_GameDraft1_WFA.Properties.Resources.gokuPortrait;
            this.pictureBox1.Location = new System.Drawing.Point(32, 5);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(207, 205);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::DBS_GameDraft1_WFA.Properties.Resources.vegetaPortrait;
            this.pictureBox4.Location = new System.Drawing.Point(616, 5);
            this.pictureBox4.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(207, 205);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 3;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(324, 280);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(207, 205);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = global::DBS_GameDraft1_WFA.Properties.Resources.WhisPortrait;
            this.pictureBox6.Location = new System.Drawing.Point(616, 280);
            this.pictureBox6.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(207, 205);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox6.TabIndex = 5;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::DBS_GameDraft1_WFA.Properties.Resources.beerus2Portrait;
            this.pictureBox5.Location = new System.Drawing.Point(32, 280);
            this.pictureBox5.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(207, 205);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox5.TabIndex = 4;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(324, 5);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(207, 205);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 2;
            this.pictureBox3.TabStop = false;
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage2.Controls.Add(this.pictureBox11);
            this.tabPage2.Controls.Add(this.Android17Btn);
            this.tabPage2.Controls.Add(this.HitBtn);
            this.tabPage2.Controls.Add(this.FriezaBtn);
            this.tabPage2.Controls.Add(this.ZamasuBtn);
            this.tabPage2.Controls.Add(this.GohanBtn);
            this.tabPage2.Controls.Add(this.BlackBtn);
            this.tabPage2.Controls.Add(this.pictureBox12);
            this.tabPage2.Controls.Add(this.pictureBox9);
            this.tabPage2.Controls.Add(this.pictureBox10);
            this.tabPage2.Controls.Add(this.pictureBox8);
            this.tabPage2.Controls.Add(this.pictureBox7);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(870, 541);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Page 2";
            // 
            // pictureBox11
            // 
            this.pictureBox11.Image = global::DBS_GameDraft1_WFA.Properties.Resources.android17Portrait;
            this.pictureBox11.Location = new System.Drawing.Point(616, 280);
            this.pictureBox11.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(207, 205);
            this.pictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox11.TabIndex = 33;
            this.pictureBox11.TabStop = false;
            // 
            // Android17Btn
            // 
            this.Android17Btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Android17Btn.Location = new System.Drawing.Point(665, 489);
            this.Android17Btn.Margin = new System.Windows.Forms.Padding(2);
            this.Android17Btn.Name = "Android17Btn";
            this.Android17Btn.Size = new System.Drawing.Size(117, 46);
            this.Android17Btn.TabIndex = 32;
            this.Android17Btn.Text = "Android 17";
            this.Android17Btn.UseVisualStyleBackColor = true;
            this.Android17Btn.Click += new System.EventHandler(this.SelectCharacter);
            // 
            // HitBtn
            // 
            this.HitBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HitBtn.Location = new System.Drawing.Point(375, 489);
            this.HitBtn.Margin = new System.Windows.Forms.Padding(2);
            this.HitBtn.Name = "HitBtn";
            this.HitBtn.Size = new System.Drawing.Size(106, 46);
            this.HitBtn.TabIndex = 30;
            this.HitBtn.Text = "Hit";
            this.HitBtn.UseVisualStyleBackColor = true;
            this.HitBtn.Click += new System.EventHandler(this.SelectCharacter);
            // 
            // FriezaBtn
            // 
            this.FriezaBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FriezaBtn.Location = new System.Drawing.Point(375, 214);
            this.FriezaBtn.Margin = new System.Windows.Forms.Padding(2);
            this.FriezaBtn.Name = "FriezaBtn";
            this.FriezaBtn.Size = new System.Drawing.Size(106, 46);
            this.FriezaBtn.TabIndex = 28;
            this.FriezaBtn.Text = "Frieza";
            this.FriezaBtn.UseVisualStyleBackColor = true;
            this.FriezaBtn.Click += new System.EventHandler(this.SelectCharacter);
            // 
            // ZamasuBtn
            // 
            this.ZamasuBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ZamasuBtn.Location = new System.Drawing.Point(665, 214);
            this.ZamasuBtn.Margin = new System.Windows.Forms.Padding(2);
            this.ZamasuBtn.Name = "ZamasuBtn";
            this.ZamasuBtn.Size = new System.Drawing.Size(117, 46);
            this.ZamasuBtn.TabIndex = 26;
            this.ZamasuBtn.Text = "Zamasu";
            this.ZamasuBtn.UseVisualStyleBackColor = true;
            this.ZamasuBtn.Click += new System.EventHandler(this.SelectCharacter);
            // 
            // GohanBtn
            // 
            this.GohanBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GohanBtn.Location = new System.Drawing.Point(76, 489);
            this.GohanBtn.Margin = new System.Windows.Forms.Padding(2);
            this.GohanBtn.Name = "GohanBtn";
            this.GohanBtn.Size = new System.Drawing.Size(114, 46);
            this.GohanBtn.TabIndex = 24;
            this.GohanBtn.Text = "Gohan";
            this.GohanBtn.UseVisualStyleBackColor = true;
            this.GohanBtn.Click += new System.EventHandler(this.SelectCharacter);
            // 
            // BlackBtn
            // 
            this.BlackBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BlackBtn.Location = new System.Drawing.Point(68, 214);
            this.BlackBtn.Margin = new System.Windows.Forms.Padding(2);
            this.BlackBtn.Name = "BlackBtn";
            this.BlackBtn.Size = new System.Drawing.Size(122, 46);
            this.BlackBtn.TabIndex = 23;
            this.BlackBtn.Text = "Goku Black";
            this.BlackBtn.UseVisualStyleBackColor = true;
            this.BlackBtn.Click += new System.EventHandler(this.SelectCharacter);
            // 
            // pictureBox12
            // 
            this.pictureBox12.Image = global::DBS_GameDraft1_WFA.Properties.Resources.hit2Portrait;
            this.pictureBox12.Location = new System.Drawing.Point(324, 280);
            this.pictureBox12.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(207, 205);
            this.pictureBox12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox12.TabIndex = 31;
            this.pictureBox12.TabStop = false;
            // 
            // pictureBox9
            // 
            this.pictureBox9.Image = global::DBS_GameDraft1_WFA.Properties.Resources.frieza2Portrait;
            this.pictureBox9.Location = new System.Drawing.Point(324, 5);
            this.pictureBox9.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(207, 205);
            this.pictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox9.TabIndex = 29;
            this.pictureBox9.TabStop = false;
            // 
            // pictureBox10
            // 
            this.pictureBox10.Image = global::DBS_GameDraft1_WFA.Properties.Resources.zamasu2Portrait;
            this.pictureBox10.Location = new System.Drawing.Point(616, 5);
            this.pictureBox10.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(207, 205);
            this.pictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox10.TabIndex = 27;
            this.pictureBox10.TabStop = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.Image = global::DBS_GameDraft1_WFA.Properties.Resources.teengohanPortrait;
            this.pictureBox8.Location = new System.Drawing.Point(32, 280);
            this.pictureBox8.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(207, 205);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox8.TabIndex = 25;
            this.pictureBox8.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = global::DBS_GameDraft1_WFA.Properties.Resources.black2Portrait;
            this.pictureBox7.Location = new System.Drawing.Point(32, 5);
            this.pictureBox7.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(207, 205);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox7.TabIndex = 23;
            this.pictureBox7.TabStop = false;
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage3.Controls.Add(this.pictureBox13);
            this.tabPage3.Controls.Add(this.TrunksBtn);
            this.tabPage3.Controls.Add(this.SasukeBtn);
            this.tabPage3.Controls.Add(this.ToppoBtn);
            this.tabPage3.Controls.Add(this.MadaraBtn);
            this.tabPage3.Controls.Add(this.KeflaBtn);
            this.tabPage3.Controls.Add(this.NarutoBtn);
            this.tabPage3.Controls.Add(this.pictureBox14);
            this.tabPage3.Controls.Add(this.pictureBox15);
            this.tabPage3.Controls.Add(this.pictureBox16);
            this.tabPage3.Controls.Add(this.pictureBox17);
            this.tabPage3.Controls.Add(this.pictureBox18);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(870, 541);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Page 3";
            // 
            // pictureBox13
            // 
            this.pictureBox13.Image = global::DBS_GameDraft1_WFA.Properties.Resources.trunks2Portrait;
            this.pictureBox13.Location = new System.Drawing.Point(616, 5);
            this.pictureBox13.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(207, 205);
            this.pictureBox13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox13.TabIndex = 45;
            this.pictureBox13.TabStop = false;
            // 
            // TrunksBtn
            // 
            this.TrunksBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TrunksBtn.Location = new System.Drawing.Point(666, 214);
            this.TrunksBtn.Margin = new System.Windows.Forms.Padding(2);
            this.TrunksBtn.Name = "TrunksBtn";
            this.TrunksBtn.Size = new System.Drawing.Size(117, 46);
            this.TrunksBtn.TabIndex = 44;
            this.TrunksBtn.Text = "Trunks";
            this.TrunksBtn.UseVisualStyleBackColor = true;
            this.TrunksBtn.Click += new System.EventHandler(this.SelectCharacter);
            // 
            // SasukeBtn
            // 
            this.SasukeBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SasukeBtn.Location = new System.Drawing.Point(666, 489);
            this.SasukeBtn.Margin = new System.Windows.Forms.Padding(2);
            this.SasukeBtn.Name = "SasukeBtn";
            this.SasukeBtn.Size = new System.Drawing.Size(117, 46);
            this.SasukeBtn.TabIndex = 42;
            this.SasukeBtn.Text = "Sasuke";
            this.SasukeBtn.UseVisualStyleBackColor = true;
            this.SasukeBtn.Click += new System.EventHandler(this.SelectCharacter);
            // 
            // ToppoBtn
            // 
            this.ToppoBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ToppoBtn.Location = new System.Drawing.Point(376, 214);
            this.ToppoBtn.Margin = new System.Windows.Forms.Padding(2);
            this.ToppoBtn.Name = "ToppoBtn";
            this.ToppoBtn.Size = new System.Drawing.Size(106, 46);
            this.ToppoBtn.TabIndex = 40;
            this.ToppoBtn.Text = "Toppo";
            this.ToppoBtn.UseVisualStyleBackColor = true;
            this.ToppoBtn.Click += new System.EventHandler(this.SelectCharacter);
            // 
            // MadaraBtn
            // 
            this.MadaraBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MadaraBtn.Location = new System.Drawing.Point(376, 489);
            this.MadaraBtn.Margin = new System.Windows.Forms.Padding(2);
            this.MadaraBtn.Name = "MadaraBtn";
            this.MadaraBtn.Size = new System.Drawing.Size(106, 46);
            this.MadaraBtn.TabIndex = 38;
            this.MadaraBtn.Text = "Madara";
            this.MadaraBtn.UseVisualStyleBackColor = true;
            this.MadaraBtn.Click += new System.EventHandler(this.SelectCharacter);
            // 
            // KeflaBtn
            // 
            this.KeflaBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.KeflaBtn.Location = new System.Drawing.Point(76, 214);
            this.KeflaBtn.Margin = new System.Windows.Forms.Padding(2);
            this.KeflaBtn.Name = "KeflaBtn";
            this.KeflaBtn.Size = new System.Drawing.Size(114, 46);
            this.KeflaBtn.TabIndex = 36;
            this.KeflaBtn.Text = "Kefla";
            this.KeflaBtn.UseVisualStyleBackColor = true;
            this.KeflaBtn.Click += new System.EventHandler(this.SelectCharacter);
            // 
            // NarutoBtn
            // 
            this.NarutoBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NarutoBtn.Location = new System.Drawing.Point(76, 489);
            this.NarutoBtn.Margin = new System.Windows.Forms.Padding(2);
            this.NarutoBtn.Name = "NarutoBtn";
            this.NarutoBtn.Size = new System.Drawing.Size(114, 46);
            this.NarutoBtn.TabIndex = 34;
            this.NarutoBtn.Text = "Naruto";
            this.NarutoBtn.UseVisualStyleBackColor = true;
            this.NarutoBtn.Click += new System.EventHandler(this.SelectCharacter);
            // 
            // pictureBox14
            // 
            this.pictureBox14.Image = global::DBS_GameDraft1_WFA.Properties.Resources.Sasuke2Portrait;
            this.pictureBox14.Location = new System.Drawing.Point(616, 280);
            this.pictureBox14.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(207, 205);
            this.pictureBox14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox14.TabIndex = 43;
            this.pictureBox14.TabStop = false;
            // 
            // pictureBox15
            // 
            this.pictureBox15.Image = global::DBS_GameDraft1_WFA.Properties.Resources.toppo3Portrait;
            this.pictureBox15.Location = new System.Drawing.Point(324, 5);
            this.pictureBox15.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(207, 205);
            this.pictureBox15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox15.TabIndex = 41;
            this.pictureBox15.TabStop = false;
            // 
            // pictureBox16
            // 
            this.pictureBox16.Image = global::DBS_GameDraft1_WFA.Properties.Resources.madaraPortrait;
            this.pictureBox16.Location = new System.Drawing.Point(324, 280);
            this.pictureBox16.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.Size = new System.Drawing.Size(207, 205);
            this.pictureBox16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox16.TabIndex = 39;
            this.pictureBox16.TabStop = false;
            // 
            // pictureBox17
            // 
            this.pictureBox17.Image = global::DBS_GameDraft1_WFA.Properties.Resources.keflaPortrait;
            this.pictureBox17.Location = new System.Drawing.Point(32, 5);
            this.pictureBox17.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox17.Name = "pictureBox17";
            this.pictureBox17.Size = new System.Drawing.Size(207, 205);
            this.pictureBox17.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox17.TabIndex = 37;
            this.pictureBox17.TabStop = false;
            // 
            // pictureBox18
            // 
            this.pictureBox18.Image = global::DBS_GameDraft1_WFA.Properties.Resources.NarutoPortrait;
            this.pictureBox18.Location = new System.Drawing.Point(32, 280);
            this.pictureBox18.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox18.Name = "pictureBox18";
            this.pictureBox18.Size = new System.Drawing.Size(207, 205);
            this.pictureBox18.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox18.TabIndex = 35;
            this.pictureBox18.TabStop = false;
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage4.Controls.Add(this.pictureBox19);
            this.tabPage4.Controls.Add(this.GrayBtn);
            this.tabPage4.Controls.Add(this.SupermanBtn);
            this.tabPage4.Controls.Add(this.NatsuBtn);
            this.tabPage4.Controls.Add(this.ZenoBtn);
            this.tabPage4.Controls.Add(this.JinBtn);
            this.tabPage4.Controls.Add(this.SaitamaBtn);
            this.tabPage4.Controls.Add(this.pictureBox20);
            this.tabPage4.Controls.Add(this.pictureBox21);
            this.tabPage4.Controls.Add(this.pictureBox22);
            this.tabPage4.Controls.Add(this.pictureBox23);
            this.tabPage4.Controls.Add(this.pictureBox24);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(870, 541);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Page 4";
            // 
            // pictureBox19
            // 
            this.pictureBox19.Image = global::DBS_GameDraft1_WFA.Properties.Resources.GrayPortrait;
            this.pictureBox19.Location = new System.Drawing.Point(616, 280);
            this.pictureBox19.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox19.Name = "pictureBox19";
            this.pictureBox19.Size = new System.Drawing.Size(207, 205);
            this.pictureBox19.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox19.TabIndex = 45;
            this.pictureBox19.TabStop = false;
            // 
            // GrayBtn
            // 
            this.GrayBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GrayBtn.Location = new System.Drawing.Point(665, 489);
            this.GrayBtn.Margin = new System.Windows.Forms.Padding(2);
            this.GrayBtn.Name = "GrayBtn";
            this.GrayBtn.Size = new System.Drawing.Size(117, 46);
            this.GrayBtn.TabIndex = 44;
            this.GrayBtn.Text = "Gray";
            this.GrayBtn.UseVisualStyleBackColor = true;
            this.GrayBtn.Click += new System.EventHandler(this.SelectCharacter);
            // 
            // SupermanBtn
            // 
            this.SupermanBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SupermanBtn.Location = new System.Drawing.Point(665, 214);
            this.SupermanBtn.Margin = new System.Windows.Forms.Padding(2);
            this.SupermanBtn.Name = "SupermanBtn";
            this.SupermanBtn.Size = new System.Drawing.Size(117, 46);
            this.SupermanBtn.TabIndex = 42;
            this.SupermanBtn.Text = "Superman";
            this.SupermanBtn.UseVisualStyleBackColor = true;
            this.SupermanBtn.Click += new System.EventHandler(this.SelectCharacter);
            // 
            // NatsuBtn
            // 
            this.NatsuBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NatsuBtn.Location = new System.Drawing.Point(76, 490);
            this.NatsuBtn.Margin = new System.Windows.Forms.Padding(2);
            this.NatsuBtn.Name = "NatsuBtn";
            this.NatsuBtn.Size = new System.Drawing.Size(114, 46);
            this.NatsuBtn.TabIndex = 40;
            this.NatsuBtn.Text = "Natsu";
            this.NatsuBtn.UseVisualStyleBackColor = true;
            this.NatsuBtn.Click += new System.EventHandler(this.SelectCharacter);
            // 
            // ZenoBtn
            // 
            this.ZenoBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ZenoBtn.Location = new System.Drawing.Point(378, 214);
            this.ZenoBtn.Margin = new System.Windows.Forms.Padding(2);
            this.ZenoBtn.Name = "ZenoBtn";
            this.ZenoBtn.Size = new System.Drawing.Size(106, 46);
            this.ZenoBtn.TabIndex = 38;
            this.ZenoBtn.Text = "Zeno";
            this.ZenoBtn.UseVisualStyleBackColor = true;
            // 
            // JinBtn
            // 
            this.JinBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JinBtn.Location = new System.Drawing.Point(378, 489);
            this.JinBtn.Margin = new System.Windows.Forms.Padding(2);
            this.JinBtn.Name = "JinBtn";
            this.JinBtn.Size = new System.Drawing.Size(106, 46);
            this.JinBtn.TabIndex = 36;
            this.JinBtn.Text = "Jin";
            this.JinBtn.UseVisualStyleBackColor = true;
            this.JinBtn.Click += new System.EventHandler(this.SelectCharacter);
            // 
            // SaitamaBtn
            // 
            this.SaitamaBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SaitamaBtn.Location = new System.Drawing.Point(76, 214);
            this.SaitamaBtn.Margin = new System.Windows.Forms.Padding(2);
            this.SaitamaBtn.Name = "SaitamaBtn";
            this.SaitamaBtn.Size = new System.Drawing.Size(114, 46);
            this.SaitamaBtn.TabIndex = 34;
            this.SaitamaBtn.Text = "Saitama";
            this.SaitamaBtn.UseVisualStyleBackColor = true;
            this.SaitamaBtn.Click += new System.EventHandler(this.SelectCharacter);
            // 
            // pictureBox20
            // 
            this.pictureBox20.Image = global::DBS_GameDraft1_WFA.Properties.Resources.supermanPortrait;
            this.pictureBox20.Location = new System.Drawing.Point(616, 5);
            this.pictureBox20.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox20.Name = "pictureBox20";
            this.pictureBox20.Size = new System.Drawing.Size(207, 205);
            this.pictureBox20.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox20.TabIndex = 43;
            this.pictureBox20.TabStop = false;
            // 
            // pictureBox21
            // 
            this.pictureBox21.Image = global::DBS_GameDraft1_WFA.Properties.Resources.natsuPortrait;
            this.pictureBox21.Location = new System.Drawing.Point(32, 281);
            this.pictureBox21.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox21.Name = "pictureBox21";
            this.pictureBox21.Size = new System.Drawing.Size(207, 205);
            this.pictureBox21.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox21.TabIndex = 41;
            this.pictureBox21.TabStop = false;
            // 
            // pictureBox22
            // 
            this.pictureBox22.Image = global::DBS_GameDraft1_WFA.Properties.Resources.zenoPortrait;
            this.pictureBox22.Location = new System.Drawing.Point(324, 5);
            this.pictureBox22.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox22.Name = "pictureBox22";
            this.pictureBox22.Size = new System.Drawing.Size(207, 205);
            this.pictureBox22.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox22.TabIndex = 39;
            this.pictureBox22.TabStop = false;
            // 
            // pictureBox23
            // 
            this.pictureBox23.Image = global::DBS_GameDraft1_WFA.Properties.Resources.jinPortrait;
            this.pictureBox23.Location = new System.Drawing.Point(324, 280);
            this.pictureBox23.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox23.Name = "pictureBox23";
            this.pictureBox23.Size = new System.Drawing.Size(207, 205);
            this.pictureBox23.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox23.TabIndex = 37;
            this.pictureBox23.TabStop = false;
            // 
            // pictureBox24
            // 
            this.pictureBox24.Image = global::DBS_GameDraft1_WFA.Properties.Resources.saitamaPortrait;
            this.pictureBox24.Location = new System.Drawing.Point(32, 5);
            this.pictureBox24.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox24.Name = "pictureBox24";
            this.pictureBox24.Size = new System.Drawing.Size(207, 205);
            this.pictureBox24.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox24.TabIndex = 35;
            this.pictureBox24.TabStop = false;
            // 
            // Character_Selection_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(902, 660);
            this.Controls.Add(this.p1readyBtn);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Character_Selection_Form";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Character Select";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).EndInit();
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox24)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button p1readyBtn;
        private System.Windows.Forms.Button GokuBtn;
        private System.Windows.Forms.Button VegetaBtn;
        private System.Windows.Forms.Button JirenBtn;
        private System.Windows.Forms.Button BeerusBtn;
        private System.Windows.Forms.Button VegitoBtn;
        private System.Windows.Forms.Button WhisBtn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.Button Android17Btn;
        private System.Windows.Forms.Button HitBtn;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.Button FriezaBtn;
        private System.Windows.Forms.Button ZamasuBtn;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.Button GohanBtn;
        private System.Windows.Forms.Button BlackBtn;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.Button TrunksBtn;
        private System.Windows.Forms.Button SasukeBtn;
        private System.Windows.Forms.Button ToppoBtn;
        private System.Windows.Forms.Button MadaraBtn;
        private System.Windows.Forms.Button KeflaBtn;
        private System.Windows.Forms.Button NarutoBtn;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.PictureBox pictureBox15;
        private System.Windows.Forms.PictureBox pictureBox16;
        private System.Windows.Forms.PictureBox pictureBox17;
        private System.Windows.Forms.PictureBox pictureBox18;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.PictureBox pictureBox19;
        private System.Windows.Forms.Button GrayBtn;
        private System.Windows.Forms.Button SupermanBtn;
        private System.Windows.Forms.Button NatsuBtn;
        private System.Windows.Forms.Button ZenoBtn;
        private System.Windows.Forms.Button JinBtn;
        private System.Windows.Forms.Button SaitamaBtn;
        private System.Windows.Forms.PictureBox pictureBox20;
        private System.Windows.Forms.PictureBox pictureBox21;
        private System.Windows.Forms.PictureBox pictureBox22;
        private System.Windows.Forms.PictureBox pictureBox23;
        private System.Windows.Forms.PictureBox pictureBox24;
    }
}