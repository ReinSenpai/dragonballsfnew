﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBS_GameDraft1_WFA
{
    public class Fighters
    {
        private string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }


        public int Health { get; set; }
        public int Power { get; set; }
        public int Damage { get; set; }
        public int Ult { get; set; }
        public int PowerCost { get; set; }
        public int UltCost { get; set; }
        public int Heal { get; set; }
        public int Form { get; set; }
        public bool Upgradable { get; set; }
        public int TransformCost { get; set; }
        public bool CanUlt { get; set; }
        public int PassiveChance { get; set; }
        public bool PassiveDodge { get; set; }
        public bool PassiveDoubleDamage { get; set; }
        public bool PassiveHalfDamage { get; set; }
        public bool PassiveTriplePowerup { get; set; }
        public bool PassiveSteal { get; set; }
        public bool PassiveAbsorb { get; set; }
        public bool PassiveUnblockable { get; set; }
        public bool PassiveSkip { get; set; }
        public Image PortraitLeft { get; set; }
        public Image PortraitRight { get; set; }

        public Fighters(string name, int health, int power, int damage, int ult, int powercost, int ultcost, int heal, int form, bool upgradable, int transformcost, bool canult, int passiveChance, bool passiveDodge, bool passiveDoubleDamage, bool passiveHalfDamage, bool passiveTriplePowerup, bool passiveSteal, bool passiveAbsorb, bool passiveUnblockable, bool passiveSkip, Image portraitLeft, Image portraitRight)
        {
            Name = name;
            Power = 70;
            Damage = damage;
            Ult = ult;
            PowerCost = powercost;
            UltCost = ultcost;
            Heal = heal;
            Form = form;
            Upgradable = upgradable;
            TransformCost = transformcost;
            CanUlt = canult;
            PassiveChance = passiveChance;
            PassiveDodge = passiveDodge;
            PassiveDoubleDamage = passiveDoubleDamage;
            PassiveHalfDamage = passiveHalfDamage;
            PassiveTriplePowerup = passiveTriplePowerup;
            PassiveSteal = passiveSteal;
            PassiveAbsorb = passiveAbsorb;
            PassiveUnblockable = passiveUnblockable;
            PassiveSkip = passiveSkip;
            PortraitLeft = portraitLeft;
            PortraitRight = portraitRight;
        }

        // public string GetName() { return Name; }
        public int GetHealth() { return this.Health; }
        public int GetPower() { return this.Power; }
        public int GetDamage() { return this.Damage; }
        public int GetUlt() { return this.Ult; }
        public int GetPowerCost() { return this.PowerCost; }
        public int GetUltCost() { return this.UltCost; }
        public int GetHeal() { return this.Heal; }
        public int GetForm() { return this.Form; }
        public bool GetUpgradable() { return this.Upgradable; }
        public int GetTransformCost() { return this.TransformCost; }
        public bool GetCanUlt() { return this.CanUlt; }
        public int GetPassiveChance() { return this.PassiveChance; }
        public bool GetPassiveDodge() { return this.PassiveDodge; }
        public bool GetPassiveDoubleDamage() { return this.PassiveDoubleDamage; }
        public bool GetPassiveHalfDamage() { return this.PassiveHalfDamage; }
        public bool GetPassiveTriplePowerup() { return this.PassiveTriplePowerup; }
        public bool GetPassiveSteal() { return this.PassiveSteal; }
        public bool GetPassiveAbsorb() { return this.PassiveAbsorb; }
        public bool GetPassiveUnblockable() { return this.PassiveUnblockable; }
        public bool GetPassiveSkip() { return this.PassiveSkip; }
        public Image GetPortraitleft() { return this.PortraitLeft; }
        public Image GetPortraitRight() { return this.PortraitRight; }


        //Set health
        public void SetName(string Name) { this.Name = Name; }
        public void SetHealth(int Health)
        {
            //If the health we are trying to set is lower than 0 then just set the health as 0
            if (Health < 0)
            {
                this.Health = 0;
            }
            else
            {
                //Set the health as whatever number is passed in
                this.Health = Health;
            }
        }
        public void SetPower(int Power) { this.Power = Power; }
        public void SetDamage(int Damage) { this.Damage = Damage; }
        public void SetUlt(int Ult) { this.Ult = Ult; }
        public void SetPowerCost(int PowerCost) { this.PowerCost = PowerCost; }
        public void SetUltCost(int UltCost) { this.UltCost = UltCost; }
        public void SetHeal(int Heal) { this.Heal = Heal; }
        public void SetForm(int Form) { this.Form = Form; }
        public void SetForm(bool Upgradable) { this.Upgradable = Upgradable; }
        public void SetTransformCost(int TransformCost) { this.TransformCost = TransformCost; }
        public void SetCanUlt(bool CanUlt) { this.CanUlt = CanUlt; }
        public void SetPassiveChance(int PassiveChance) { this.PassiveChance = PassiveChance; }
        public void SetPassiveDodge(bool PassiveDodge) { this.PassiveDodge = PassiveDodge; }
        public void SetPassiveDoubleDamage(bool PassiveDoubleDamage) { this.PassiveDoubleDamage = PassiveDoubleDamage; }
        public void SetPassiveHalfDamage(bool PassiveHalfDamage) { this.PassiveHalfDamage = PassiveHalfDamage; }
        public void SetPassiveTriplePowerup(bool PassiveTriplePowerup) { this.PassiveTriplePowerup = PassiveTriplePowerup; }
        public void SetPassiveSteal(bool PassiveSteal) { this.PassiveSteal = PassiveSteal; }
        public void SetPassiveAbsorb(bool PassiveAbsorb) { this.PassiveAbsorb = PassiveAbsorb; }
        public void SetPassiveUnblockable(bool PassiveUnblockable) { this.PassiveUnblockable = PassiveUnblockable; }
        public void SetPassiveSkip(bool PassiveSkip) { this.PassiveSkip = PassiveSkip; }
        public void SetPortraitLeft(Image PortraitLeft) { this.PortraitLeft = PortraitLeft; }
        public void SetPortraitRight(Image PortraitRight) { this.PortraitRight = PortraitRight; }
    }
}
