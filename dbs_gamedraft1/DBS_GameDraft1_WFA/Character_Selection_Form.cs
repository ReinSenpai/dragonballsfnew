﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;

namespace DBS_GameDraft1_WFA
{
    public partial class Character_Selection_Form : Form
    {
        public Fighters P1;
        public Fighters P2;

        Random rnd = new Random();

        private Color P1Selected = Color.Green;
        private Color P2Selected = Color.Red;

        private List<Fighters> Fighters = new List<Fighters>();
        private Battleground_Form BattleGround;

        int clicks = 0;

        public Character_Selection_Form()
        {
            InitializeComponent();
            Fighters.Add(new Fighters("Goku", 100, 70, 20, 100, 20, 100, 0, 0, true, 10, false, 15, false, false, false, false, false, false, false, false, Properties.Resources.Goku1Left, Properties.Resources.Goku1Left)
            {
                Name = "Goku",
                Health = 100,
                Power = 70,
                Damage = 20,
                Ult = 100,
                PowerCost = 20,
                UltCost = 100,
                Heal = 0,
                Form = 0,
                Upgradable = true,
                TransformCost = 10,
                CanUlt = false,
                PassiveChance = 15,
                PassiveDodge = false,
                PassiveDoubleDamage = false,
                PassiveHalfDamage = false,
                PassiveTriplePowerup = false,
                PassiveAbsorb = false,
                PassiveSteal = false,
                PassiveUnblockable = false,
                PassiveSkip = false,
                PortraitLeft = Properties.Resources.Goku1Left,
                PortraitRight = Properties.Resources.Goku1Left,
            });
            Fighters.Add(new Fighters("Vegeta", 100, 70, 20, 80, 20, 90, 30, 0, true, 10, true, 10, false, false, false, false, false, false, false, false, Properties.Resources.vegeta1Left, Properties.Resources.vegeta1Right)
            {
                Name = "Vegeta",
                Health = 100,
                Power = 70,
                Damage = 20,
                Ult = 80,
                PowerCost = 20,
                UltCost = 90,
                Heal = 30,
                Form = 0,
                Upgradable = true,
                TransformCost = 10,
                CanUlt = true,
                PassiveChance = 10,
                PassiveDodge = false,
                PassiveDoubleDamage = false,
                PassiveHalfDamage = false,
                PassiveTriplePowerup = false,
                PassiveAbsorb = false,
                PassiveSteal = false,
                PassiveUnblockable = false,
                PassiveSkip = false,
                PortraitLeft = Properties.Resources.vegeta1Left,
                PortraitRight = Properties.Resources.vegeta1Right,

            });
            Fighters.Add(new Fighters("Jiren", 100, 70, 30, 40, 20, 70, 30, 0, true, 90, true, 25, false, false, false, false, false, false, false, false, Properties.Resources.jiren1Left, Properties.Resources.jiren1Right)
            {
                Name = "Jiren",
                Health = 100,
                Power = 70,
                Damage = 30,
                Ult = 40,
                PowerCost = 20,
                UltCost = 70,
                Heal = 30,
                Form = 0,
                Upgradable = true,
                TransformCost = 90,
                CanUlt = true,
                PassiveChance = 25,
                PassiveDodge = false,
                PassiveDoubleDamage = false,
                PassiveHalfDamage = false,
                PassiveTriplePowerup = false,
                PassiveAbsorb = false,
                PassiveSteal = false,
                PassiveUnblockable = false,
                PassiveSkip = false,
                PortraitLeft = Properties.Resources.jiren1Left,
                PortraitRight = Properties.Resources.jiren1Right,
            });
            Fighters.Add(new Fighters("Beerus", 100, 70, 30, 75, 15, 50, 0, 0, true, 90, false, 20, false, false, false, false, false, false, false, false, Properties.Resources.beerus1Left, Properties.Resources.beerus1Right)
            {
                Name = "Beerus",
                Health = 100,
                Power = 70,
                Damage = 30,
                Ult = 75,
                PowerCost = 15,
                UltCost = 50,
                Heal = 0,
                Form = 0,
                Upgradable = true,
                TransformCost = 90,
                CanUlt = false,
                PassiveChance = 20,
                PassiveDodge = false,
                PassiveDoubleDamage = false,
                PassiveHalfDamage = false,
                PassiveTriplePowerup = false,
                PassiveAbsorb = false,
                PassiveSteal = false,
                PassiveUnblockable = false,
                PassiveSkip = false,
                PortraitLeft = Properties.Resources.beerus1Left,
                PortraitRight = Properties.Resources.beerus1Right,

            });
            Fighters.Add(new Fighters("Vegito", 100, 70, 20, 90, 20, 100, 0, 0, true, 20, false, 25, false, false, false, false, false, false, false, false, Properties.Resources.vegito1Left, Properties.Resources.vegito1Right)
            {
                Name = "Vegito",
                Health = 100,
                Power = 70,
                Damage = 20,
                Ult = 90,
                PowerCost = 20,
                UltCost = 100,
                Heal = 0,
                Form = 0,
                Upgradable = true,
                TransformCost = 20,
                CanUlt = false,
                PassiveChance = 25,
                PassiveDodge = false,
                PassiveDoubleDamage = false,
                PassiveHalfDamage = false,
                PassiveTriplePowerup = false,
                PassiveAbsorb = false,
                PassiveSteal = false,
                PassiveUnblockable = false,
                PassiveSkip = false,
                PortraitLeft = Properties.Resources.vegito1Left,
                PortraitRight = Properties.Resources.vegito1Right,

            });
            Fighters.Add(new Fighters("Whis", 100, 70, 25, 100, 20, 100, 0, 0, true, 90, false, 50, false, false, false, false, false, false, false, false, Properties.Resources.whis1Left, Properties.Resources.whis1Right)
            {
                Name = "Whis",
                Health = 100,
                Power = 70,
                Damage = 25,
                Ult = 100,
                PowerCost = 20,
                UltCost = 100,
                Heal = 0,
                Form = 0,
                Upgradable = true,
                TransformCost = 90,
                CanUlt = false,
                PassiveChance = 50,
                PassiveDodge = false,
                PassiveDoubleDamage = false,
                PassiveHalfDamage = false,
                PassiveTriplePowerup = false,
                PassiveAbsorb = false,
                PassiveSteal = false,
                PassiveUnblockable = false,
                PassiveSkip = false,
                PortraitLeft = Properties.Resources.whis1Left,
                PortraitRight = Properties.Resources.whis1Right,
            });
            Fighters.Add(new Fighters("Superman", 100, 70, 20, 70, 20, 70, 0, 0, false, 0, true, 0, false, false, false, false, false, false, false, false, Properties.Resources.superman1Left, Properties.Resources.superman1Right)
            {
                Name = "Superman",
                Health = 100,
                Power = 70,
                Damage = 20,
                Ult = 70,
                PowerCost = 20,
                UltCost = 70,
                Heal = 0,
                Form = 0,
                Upgradable = false,
                TransformCost = 0,
                CanUlt = true,
                PassiveChance = 0,
                PassiveDodge = false,
                PassiveDoubleDamage = false,
                PassiveHalfDamage = false,
                PassiveTriplePowerup = false,
                PassiveAbsorb = false,
                PassiveSteal = false,
                PassiveUnblockable = false,
                PassiveSkip = false,
                PortraitLeft = Properties.Resources.superman1Left,
                PortraitRight = Properties.Resources.superman1Right,
            });
            Fighters.Add(new Fighters("Saitama", 100, 70, 25, 100, 20, 100, 0, 0, false, 0, true, 0, false, false, false, false, false, false, false, false, Properties.Resources.saitama1Left, Properties.Resources.saitama1Right)
            {
                Name = "Saitama",
                Health = 100,
                Power = 70,
                Damage = 25,
                Ult = 100,
                PowerCost = 20,
                UltCost = 100,
                Heal = 0,
                Form = 0,
                Upgradable = false,
                TransformCost = 0,
                CanUlt = true,
                PassiveChance = 0,
                PassiveDodge = false,
                PassiveDoubleDamage = false,
                PassiveHalfDamage = false,
                PassiveTriplePowerup = false,
                PassiveAbsorb = false,
                PassiveSteal = false,
                PassiveUnblockable = false,
                PassiveSkip = false,
                PortraitLeft = Properties.Resources.saitama1Left,
                PortraitRight = Properties.Resources.saitama1Right,
            });
            Fighters.Add(new Fighters("Gohan", 100, 70, 15, 95, 20, 90, 35, 0, true, 10, false, 15, false, false, false, false, false, false, false, false, Properties.Resources.gohan1Left, Properties.Resources.gohan1Right)
            {
                Name = "Gohan",
                Health = 100,
                Power = 70,
                Damage = 15,
                Ult = 95,
                PowerCost = 20,
                UltCost = 90,
                Heal = 35,
                Form = 0,
                Upgradable = true,
                TransformCost = 10,
                CanUlt = false,
                PassiveChance = 15,
                PassiveDodge = false,
                PassiveDoubleDamage = false,
                PassiveHalfDamage = false,
                PassiveTriplePowerup = false,
                PassiveAbsorb = false,
                PassiveSteal = false,
                PassiveUnblockable = false,
                PassiveSkip = false,
                PortraitLeft = Properties.Resources.gohan1Left,
                PortraitRight = Properties.Resources.gohan1Right,
            });
            Fighters.Add(new Fighters("Frieza", 100, 70, 20, 50, 20, 60, 0, 0, true, 90, false, 35, false, false, false, false, false, false, false, false, Properties.Resources.frieza1Left, Properties.Resources.frieza1Right)
            {
                Name = "Frieza",
                Health = 100,
                Power = 70,
                Damage = 20,
                Ult = 50,
                PowerCost = 20,
                UltCost = 60,
                Heal = 0,
                Form = 0,
                Upgradable = true,
                TransformCost = 90,
                CanUlt = false,
                PassiveChance = 35,
                PassiveDodge = false,
                PassiveDoubleDamage = false,
                PassiveHalfDamage = false,
                PassiveTriplePowerup = false,
                PassiveAbsorb = false,
                PassiveSteal = false,
                PassiveUnblockable = false,
                PassiveSkip = false,
                PortraitLeft = Properties.Resources.frieza1Left,
                PortraitRight = Properties.Resources.frieza1Right,
            });
            Fighters.Add(new Fighters("Android 17", 100, 70, 15, 45, 15, 50, 10, 0, true, 70, false, 15, false, false, false, false, false, false, false, false, Properties.Resources.android1Left, Properties.Resources.android1Right)
            {
                Name = "Android 17",
                Health = 100,
                Power = 70,
                Damage = 15,
                Ult = 45,
                PowerCost = 15,
                UltCost = 50,
                Heal = 10,
                Form = 0,
                Upgradable = true,
                TransformCost = 70,
                CanUlt = false,
                PassiveChance = 15,
                PassiveDodge = false,
                PassiveDoubleDamage = false,
                PassiveHalfDamage = false,
                PassiveTriplePowerup = false,
                PassiveAbsorb = false,
                PassiveSteal = false,
                PassiveUnblockable = false,
                PassiveSkip = false,
                PortraitLeft = Properties.Resources.android1Left,
                PortraitRight = Properties.Resources.android1Right,
            });
            Fighters.Add(new Fighters("Sasuke", 100, 70, 20, 55, 20, 50, 0, 0, true, 50, false, 15, false, false, false, false, false, false, false, false, Properties.Resources.sasuke1Left2, Properties.Resources.sasuke1Right)
            {
                Name = "Sasuke",
                Health = 100,
                Power = 70,
                Damage = 20,
                Ult = 55,
                PowerCost = 20,
                UltCost = 50,
                Heal = 0,
                Form = 0,
                Upgradable = true,
                TransformCost = 50,
                CanUlt = false,
                PassiveChance = 15,
                PassiveDodge = false,
                PassiveDoubleDamage = false,
                PassiveHalfDamage = false,
                PassiveTriplePowerup = false,
                PassiveSteal = false,
                PassiveAbsorb = false,
                PassiveUnblockable = false,
                PassiveSkip = false,
                PortraitLeft = Properties.Resources.sasuke1Left2,
                PortraitRight = Properties.Resources.sasuke1Right,
            });
            Fighters.Add(new Fighters("Goku Black", 100, 70, 20, 100, 20, 100, 0, 0, true, 20, false, 0, false, false, false, false, false, false, false, false, Properties.Resources.black1left, Properties.Resources.black1Right)
            {
                Name = "Goku Black",
                Health = 100,
                Power = 70,
                Damage = 20,
                Ult = 100,
                PowerCost = 20,
                UltCost = 100,
                Heal = 0,
                Form = 0,
                Upgradable = true,
                TransformCost = 20,
                CanUlt = false,
                PassiveChance = 0,
                PassiveDodge = false,
                PassiveDoubleDamage = false,
                PassiveHalfDamage = false,
                PassiveTriplePowerup = false,
                PassiveAbsorb = false,
                PassiveSteal = false,
                PassiveUnblockable = false,
                PassiveSkip = false,
                PortraitLeft = Properties.Resources.black1left,
                PortraitRight = Properties.Resources.black1Right,
            });
            Fighters.Add(new Fighters("Zamasu", 100, 70, 20, 70, 20, 70, 0, 0, true, 80, false, 0, false, false, false, false, false, false, false, false, Properties.Resources.zamasu1Left, Properties.Resources.zamasu1Right)
            {
                Name = "Zamasu",
                Health = 100,
                Power = 70,
                Damage = 20,
                Ult = 70,
                PowerCost = 20,
                UltCost = 70,
                Heal = 0,
                Form = 0,
                Upgradable = true,
                TransformCost = 80,
                CanUlt = false,
                PassiveChance = 0,
                PassiveDodge = false,
                PassiveDoubleDamage = false,
                PassiveHalfDamage = false,
                PassiveTriplePowerup = false,
                PassiveAbsorb = false,
                PassiveSteal = false,
                PassiveUnblockable = false,
                PassiveSkip = false,
                PortraitLeft = Properties.Resources.zamasu1Left,
                PortraitRight = Properties.Resources.zamasu1Right,

            });
            Fighters.Add(new Fighters("Hit", 100, 70, 30, 60, 20, 70, 0, 0, true, 90, true, 35, false, false, false, false, false, false, false, false, Properties.Resources.hit1Left, Properties.Resources.hit1Right)
            {
                Name = "Hit",
                Health = 100,
                Power = 70,
                Damage = 30,
                Ult = 60,
                PowerCost = 20,
                UltCost = 70,
                Heal = 0,
                Form = 0,
                Upgradable = true,
                TransformCost = 90,
                CanUlt = true,
                PassiveChance = 35,
                PassiveDodge = false,
                PassiveDoubleDamage = false,
                PassiveHalfDamage = false,
                PassiveTriplePowerup = false,
                PassiveAbsorb = false,
                PassiveSteal = false,
                PassiveUnblockable = false,
                PassiveSkip = false,
                PortraitLeft = Properties.Resources.hit1Left,
                PortraitRight = Properties.Resources.hit1Right,
            });
            Fighters.Add(new Fighters("Kefla", 100, 70, 20, 70, 20, 70, 0, 0, true, 80, false, 0, false, false, false, false, false, false, false, false, Properties.Resources.kefla1Left, Properties.Resources.kefla1Left)
            {
                Name = "Kefla",
                Health = 100,
                Power = 70,
                Damage = 20,
                Ult = 70,
                PowerCost = 20,
                UltCost = 70,
                Heal = 0,
                Form = 0,
                Upgradable = true,
                TransformCost = 80,
                CanUlt = false,
                PassiveChance = 0,
                PassiveDodge = false,
                PassiveDoubleDamage = false,
                PassiveHalfDamage = false,
                PassiveTriplePowerup = false,
                PassiveAbsorb = false,
                PassiveSteal = false,
                PassiveUnblockable = false,
                PassiveSkip = false,
                PortraitLeft = Properties.Resources.kefla1Left,
                PortraitRight = Properties.Resources.kefla1Left,
            });
            Fighters.Add(new Fighters("Toppo", 100, 70, 20, 70, 20, 70, 0, 0, true, 80, false, 15, false, false, false, false, false, false, false, false, Properties.Resources.toppo1Left, Properties.Resources.toppo1Right)
            {
                Name = "Toppo",
                Health = 100,
                Power = 70,
                Damage = 20,
                Ult = 70,
                PowerCost = 20,
                UltCost = 70,
                Heal = 0,
                Form = 0,
                Upgradable = true,
                TransformCost = 80,
                CanUlt = false,
                PassiveChance = 15,
                PassiveDodge = false,
                PassiveDoubleDamage = false,
                PassiveHalfDamage = false,
                PassiveTriplePowerup = false,
                PassiveAbsorb = false,
                PassiveSteal = false,
                PassiveUnblockable = false,
                PassiveSkip = false,
                PortraitLeft = Properties.Resources.toppo1Left,
                PortraitRight = Properties.Resources.toppo1Right,
            });
            Fighters.Add(new Fighters("Trunks", 100, 70, 20, 70, 20, 70, 0, 0, true, 20, false, 0, false, false, false, false, false, false, false, false, Properties.Resources.trunks1Left, Properties.Resources.trunks1Right)
            {
                Name = "Trunks",
                Health = 100,
                Power = 70,
                Damage = 20,
                Ult = 70,
                PowerCost = 20,
                UltCost = 70,
                Heal = 0,
                Form = 0,
                Upgradable = true,
                TransformCost = 20,
                CanUlt = false,
                PassiveChance = 0,
                PassiveDodge = false,
                PassiveDoubleDamage = false,
                PassiveHalfDamage = false,
                PassiveTriplePowerup = false,
                PassiveAbsorb = false,
                PassiveSteal = false,
                PassiveUnblockable = false,
                PassiveSkip = false,
                PortraitLeft = Properties.Resources.trunks1Left,
                PortraitRight = Properties.Resources.trunks1Right,
            });
            Fighters.Add(new Fighters("Naruto", 100, 70, 20, 90, 20, 80, 0, 0, true, 60, false, 20, false, false, false, false, false, false, false, false, Properties.Resources.naruto1Left, Properties.Resources.naruto1Right)
            {
                Name = "Naruto",
                Health = 100,
                Power = 70,
                Damage = 20,
                Ult = 90,
                PowerCost = 20,
                UltCost = 80,
                Heal = 0,
                Form = 0,
                Upgradable = true,
                TransformCost = 60,
                CanUlt = false,
                PassiveChance = 20,
                PassiveDodge = false,
                PassiveDoubleDamage = false,
                PassiveHalfDamage = false,
                PassiveTriplePowerup = false,
                PassiveAbsorb = false,
                PassiveSteal = false,
                PassiveUnblockable = false,
                PassiveSkip = false,
                PortraitLeft = Properties.Resources.naruto1Left,
                PortraitRight = Properties.Resources.naruto1Right,
            });
            Fighters.Add(new Fighters("Madara", 100, 70, 25, 50, 20, 60, 0, 0, true, 70, false, 20, false, false, false, false, false, false, false, false, Properties.Resources.madara1Left, Properties.Resources.madara1Right)
            {
                Name = "Madara",
                Health = 100,
                Power = 70,
                Damage = 25,
                Ult = 50,
                PowerCost = 20,
                UltCost = 60,
                Heal = 0,
                Form = 0,
                Upgradable = true,
                TransformCost = 70,
                CanUlt = false,
                PassiveChance = 20,
                PassiveDodge = false,
                PassiveDoubleDamage = false,
                PassiveHalfDamage = false,
                PassiveTriplePowerup = false,
                PassiveAbsorb = false,
                PassiveSteal = false,
                PassiveUnblockable = false,
                PassiveSkip = false,
                PortraitLeft = Properties.Resources.madara1Left,
                PortraitRight = Properties.Resources.madara1Right,
            });
            Fighters.Add(new Fighters("Jin", 100, 70, 20, 70, 20, 70, 0, 0, true, 50, false, 0, false, false, false, false, false, false, false, false, Properties.Resources.jin1Left, Properties.Resources.jin1Right)
            {
                Name = "Jin",
                Health = 100,
                Power = 70,
                Damage = 20,
                Ult = 70,
                PowerCost = 20,
                UltCost = 70,
                Heal = 0,
                Form = 0,
                Upgradable = true,
                TransformCost = 50,
                CanUlt = false,
                PassiveChance = 0,
                PassiveDodge = false,
                PassiveDoubleDamage = false,
                PassiveHalfDamage = false,
                PassiveTriplePowerup = false,
                PassiveAbsorb = false,
                PassiveSteal = false,
                PassiveUnblockable = false,
                PassiveSkip = false,
                PortraitLeft = Properties.Resources.jin1Left,
                PortraitRight = Properties.Resources.jin1Right,
            });
            Fighters.Add(new Fighters("Natsu", 100, 70, 20, 70, 20, 70, 0, 0, true, 70, false, 20, false, false, false, false, false, false, false, false, Properties.Resources.natsu1Left, Properties.Resources.natsu1Right)
            {
                Name = "Natsu",
                Health = 100,
                Power = 70,
                Damage = 20,
                Ult = 70,
                PowerCost = 20,
                UltCost = 70,
                Heal = 0,
                Form = 0,
                Upgradable = true,
                TransformCost = 70,
                CanUlt = false,
                PassiveChance = 20,
                PassiveDodge = false,
                PassiveDoubleDamage = false,
                PassiveHalfDamage = false,
                PassiveTriplePowerup = false,
                PassiveAbsorb = false,
                PassiveSteal = false,
                PassiveUnblockable = false,
                PassiveSkip = false,
                PortraitLeft = Properties.Resources.natsu1Left,
                PortraitRight = Properties.Resources.natsu1Right,
            });
            Fighters.Add(new Fighters("Gray", 100, 70, 20, 70, 20, 70, 0, 0, true, 70, false, 20, false, false, false, false, false, false, false, false, Properties.Resources.gray1Left, Properties.Resources.gray1Right)
            {
                Name = "Gray",
                Health = 100,
                Power = 70,
                Damage = 20,
                Ult = 70,
                PowerCost = 20,
                UltCost = 70,
                Heal = 0,
                Form = 0,
                Upgradable = true,
                TransformCost = 70,
                CanUlt = false,
                PassiveChance = 20,
                PassiveDodge = false,
                PassiveDoubleDamage = false,
                PassiveHalfDamage = false,
                PassiveTriplePowerup = false,
                PassiveAbsorb = false,
                PassiveSteal = false,
                PassiveUnblockable = false,
                PassiveSkip = false,
                PortraitLeft = Properties.Resources.gray1Left,
                PortraitRight = Properties.Resources.gray1Right,
            });
            Fighters.Add(new Fighters("Zeno", 100, 100, 50, 100, 0, 0, 0, 0, false, 0, false, 0, false, false, false, false, false, false, false, false, Properties.Resources.zeno1Left, Properties.Resources.zeno1Right)
            {
                Name = "Zeno",
                Health = 100,
                Power = 100,
                Damage = 50,
                Ult = 100,
                PowerCost = 0,
                UltCost = 0,
                Heal = 0,
                Form = 0,
                Upgradable = false,
                TransformCost = 0,
                CanUlt = false,
                PassiveChance = 0,
                PassiveDodge = false,
                PassiveDoubleDamage = false,
                PassiveHalfDamage = false,
                PassiveTriplePowerup = false,
                PassiveAbsorb = false,
                PassiveSteal = false,
                PassiveUnblockable = false,
                PassiveSkip = false,
                PortraitLeft = Properties.Resources.zeno1Left,
                PortraitRight = Properties.Resources.zeno1Right,
            });
        }
        public void p1readyBtn_Click(object sender, EventArgs e)
        {
            if (clicks >= 2)
            {
                BattleGround = new Battleground_Form(P1, P2);
                BattleGround.Show();
                this.Close();
            }
            else
            {
                if (clicks == 0)
                {
                    MessageBox.Show("Player 1 Must Select A Character");
                }
                else if (clicks == 1)
                {
                    MessageBox.Show("Player 2 Must Select A Character");
                }
            }
        }
        public void SelectCharacter(object sender, EventArgs e)
        {
            string character = (sender as Button).Text;

            //Player 1
            if (clicks == 0)
            {
                P1 = Fighters.Find(x => x.Name.ToLower() == character.ToLower());
                (sender as Button).BackColor = P1Selected;
                clicks += 1;
                label1.Text = "Player Two Choose Your Fighter!";
            }
            else
            {
                P2 = Fighters.Find(x => x.Name.ToLower() == character.ToLower());
                (sender as Button).BackColor = P2Selected;
                clicks += 1;
                label1.Text = "Click The READY button to play!";
            }
        }
        private void label2_Click(object sender, EventArgs e) //random character function
        {
            string character = " ";

            int ran = 0;

            ran = rnd.Next(1, 24);
            if (clicks == 0)
            {
                if (ran == 1)
                {
                    character = "Goku";
                    GokuBtn.BackColor = P1Selected;
                }
                if (ran == 2)
                {
                    character = "Vegeta";
                    VegetaBtn.BackColor = P1Selected;
                }
                if (ran == 3)
                {
                    character = "Jiren";
                    JirenBtn.BackColor = P1Selected;
                }
                if (ran == 4)
                {
                    character = "Beerus";
                    BeerusBtn.BackColor = P1Selected;
                }
                if (ran == 5)
                {
                    character = "Vegito";
                    VegitoBtn.BackColor = P1Selected;
                }
                if (ran == 6)
                {
                    character = "Gohan";
                    GohanBtn.BackColor = P1Selected;
                }
                if (ran == 7)
                {
                    character = "Android 17";
                    Android17Btn.BackColor = P1Selected;
                }
                if (ran == 8)
                {
                    character = "Superman";
                    BlackBtn.BackColor = P1Selected;
                }
                if (ran == 9)
                {
                    character = "Saitama";
                    ZamasuBtn.BackColor = P1Selected;
                }
                if (ran == 10)
                {
                    character = "Frieza";
                    FriezaBtn.BackColor = P1Selected;
                }
                if (ran == 11)
                {
                    character = "Whis";
                    WhisBtn.BackColor = P1Selected;
                }
                if (ran == 12)
                {
                    character = "Sasuke";
                    SasukeBtn.BackColor = P1Selected;
                }
                if (ran == 13)
                {
                    character = "Naruto";
                    NarutoBtn.BackColor = P1Selected;
                }
                if (ran == 14)
                {
                    character = "Madara";
                    MadaraBtn.BackColor = P1Selected;
                }
                if (ran == 15)
                {
                    character = "Toppo";
                    ToppoBtn.BackColor = P1Selected;
                }
                if (ran == 16)
                {
                    character = "Trunks";
                    TrunksBtn.BackColor = P1Selected;
                }
                if (ran == 17)
                {
                    character = "Kefla";
                    KeflaBtn.BackColor = P1Selected;
                }
                if (ran == 18)
                {
                    character = "Jin";
                    JinBtn.BackColor = P1Selected;
                }
                if (ran == 19)
                {
                    character = "Gray";
                    GrayBtn.BackColor = P1Selected;
                }
                if (ran == 20)
                {
                    character = "Natsu";
                    NatsuBtn.BackColor = P1Selected;
                }
                if (ran == 21)
                {
                    character = "Zeno";
                    ZenoBtn.BackColor = P1Selected;
                }
                if (ran == 22)
                {
                    character = "Hit";
                    HitBtn.BackColor = P1Selected;
                }
                if (ran == 23)
                {
                    character = "Goku Black";
                    BlackBtn.BackColor = P1Selected;
                }
                if (ran == 24)
                {
                    character = "Zamasu";
                    ZamasuBtn.BackColor = P1Selected;
                }

            }
            if (clicks == 1)
            {
                if (ran == 1)
                {
                    character = "Goku";
                    GokuBtn.BackColor = P2Selected;
                }
                if (ran == 2)
                {
                    character = "Vegeta";
                    VegetaBtn.BackColor = P2Selected;
                }
                if (ran == 3)
                {
                    character = "Jiren";
                    JirenBtn.BackColor = P2Selected;
                }
                if (ran == 4)
                {
                    character = "Beerus";
                    BeerusBtn.BackColor = P2Selected;
                }
                if (ran == 5)
                {
                    character = "Vegito";
                    VegitoBtn.BackColor = P2Selected;
                }
                if (ran == 6)
                {
                    character = "Gohan";
                    GohanBtn.BackColor = P2Selected;
                }
                if (ran == 7)
                {
                    character = "Android 17";
                    Android17Btn.BackColor = P2Selected;
                }
                if (ran == 8)
                {
                    character = "Superman";
                    BlackBtn.BackColor = P2Selected;
                }
                if (ran == 9)
                {
                    character = "Saitama";
                    ZamasuBtn.BackColor = P2Selected;
                }
                if (ran == 10)
                {
                    character = "Frieza";
                    FriezaBtn.BackColor = P2Selected;
                }
                if (ran == 11)
                {
                    character = "Whis";
                    WhisBtn.BackColor = P2Selected;
                }
                if (ran == 12)
                {
                    character = "Sasuke";
                    HitBtn.BackColor = P2Selected;
                }
                if (ran == 13)
                {
                    character = "Naruto";
                    NarutoBtn.BackColor = P2Selected;
                }
                if (ran == 14)
                {
                    character = "Madara";
                    MadaraBtn.BackColor = P2Selected;
                }
                if (ran == 15)
                {
                    character = "Toppo";
                    ToppoBtn.BackColor = P2Selected;
                }
                if (ran == 16)
                {
                    character = "Trunks";
                    TrunksBtn.BackColor = P2Selected;
                }
                if (ran == 17)
                {
                    character = "Kefla";
                    KeflaBtn.BackColor = P2Selected;
                }
                if (ran == 18)
                {
                    character = "Jin";
                    JinBtn.BackColor = P2Selected;
                }
                if (ran == 19)
                {
                    character = "Gray";
                    GrayBtn.BackColor = P2Selected;
                }
                if (ran == 20)
                {
                    character = "Natsu";
                    NatsuBtn.BackColor = P2Selected;
                }
                if (ran == 21)
                {
                    character = "Zeno";
                    ZenoBtn.BackColor = P2Selected;
                }
                if (ran == 22)
                {
                    character = "Hit";
                    HitBtn.BackColor = P2Selected;
                }
                if (ran == 23)
                {
                    character = "Goku Black";
                    BlackBtn.BackColor = P2Selected;
                }
                if (ran == 24)
                {
                    character = "Zamasu";
                    ZamasuBtn.BackColor = P2Selected;
                }
            }
            if (clicks == 0)
            {
                P1 = Fighters.Find(x => x.Name.ToLower() == character.ToLower());
                clicks += 1;
                label1.Text = "Player Two Choose Your Fighter!";
            }
            else
            {
                P2 = Fighters.Find(x => x.Name.ToLower() == character.ToLower());
                clicks += 1;
                label1.Text = "Click The READY button to play!";
            }
        }
    }
}