﻿namespace DBS_GameDraft1_WFA
{
    partial class Battleground_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Battleground_Form));
            this.p1HealthBar = new System.Windows.Forms.ProgressBar();
            this.P1Name = new System.Windows.Forms.Label();
            this.p1PowerBar = new System.Windows.Forms.ProgressBar();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.p2PowerBar = new System.Windows.Forms.ProgressBar();
            this.P2Name = new System.Windows.Forms.Label();
            this.p2HealthBar = new System.Windows.Forms.ProgressBar();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.battledata = new System.Windows.Forms.TextBox();
            this.p1healthbarLabel = new System.Windows.Forms.Label();
            this.p2healthbarLabel = new System.Windows.Forms.Label();
            this.p1powerbarLabel = new System.Windows.Forms.Label();
            this.p2powerbarLabel = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.label6 = new System.Windows.Forms.Label();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.button7 = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.button8 = new System.Windows.Forms.Button();
            this.timer3 = new System.Windows.Forms.Timer(this.components);
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.timer4 = new System.Windows.Forms.Timer(this.components);
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.Player1RB3 = new System.Windows.Forms.CheckBox();
            this.Player1RB2 = new System.Windows.Forms.CheckBox();
            this.Player1RB1 = new System.Windows.Forms.CheckBox();
            this.Player2RB3 = new System.Windows.Forms.CheckBox();
            this.Player2RB2 = new System.Windows.Forms.CheckBox();
            this.Player2RB1 = new System.Windows.Forms.CheckBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.fighter1PicBox = new System.Windows.Forms.PictureBox();
            this.fighter2PicBox2 = new System.Windows.Forms.PictureBox();
            this.fighter1PicBox2 = new System.Windows.Forms.PictureBox();
            this.fighter2PicBox = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.fighter1PicBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fighter2PicBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fighter1PicBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fighter2PicBox)).BeginInit();
            this.SuspendLayout();
            // 
            // p1HealthBar
            // 
            this.p1HealthBar.BackColor = System.Drawing.Color.Red;
            this.p1HealthBar.ForeColor = System.Drawing.Color.Green;
            this.p1HealthBar.Location = new System.Drawing.Point(10, 523);
            this.p1HealthBar.Margin = new System.Windows.Forms.Padding(2);
            this.p1HealthBar.Name = "p1HealthBar";
            this.p1HealthBar.Size = new System.Drawing.Size(196, 27);
            this.p1HealthBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.p1HealthBar.TabIndex = 1;
            this.p1HealthBar.Value = 100;
            // 
            // P1Name
            // 
            this.P1Name.AutoSize = true;
            this.P1Name.BackColor = System.Drawing.Color.Transparent;
            this.P1Name.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.P1Name.Location = new System.Drawing.Point(158, 30);
            this.P1Name.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.P1Name.Name = "P1Name";
            this.P1Name.Size = new System.Drawing.Size(216, 31);
            this.P1Name.TabIndex = 3;
            this.P1Name.Text = "Player 1 Name...";
            // 
            // p1PowerBar
            // 
            this.p1PowerBar.BackColor = System.Drawing.SystemColors.Control;
            this.p1PowerBar.ForeColor = System.Drawing.Color.Yellow;
            this.p1PowerBar.Location = new System.Drawing.Point(10, 575);
            this.p1PowerBar.Margin = new System.Windows.Forms.Padding(2);
            this.p1PowerBar.Name = "p1PowerBar";
            this.p1PowerBar.Size = new System.Drawing.Size(196, 27);
            this.p1PowerBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.p1PowerBar.TabIndex = 4;
            this.p1PowerBar.Value = 70;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 501);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 20);
            this.label2.TabIndex = 5;
            this.label2.Text = "Health:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(11, 552);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 20);
            this.label3.TabIndex = 6;
            this.label3.Text = "Power:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(1012, 559);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 20);
            this.label1.TabIndex = 12;
            this.label1.Text = "Power:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(1012, 507);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 20);
            this.label4.TabIndex = 11;
            this.label4.Text = "Health:";
            // 
            // p2PowerBar
            // 
            this.p2PowerBar.BackColor = System.Drawing.SystemColors.Control;
            this.p2PowerBar.ForeColor = System.Drawing.Color.Yellow;
            this.p2PowerBar.Location = new System.Drawing.Point(924, 581);
            this.p2PowerBar.Margin = new System.Windows.Forms.Padding(2);
            this.p2PowerBar.Name = "p2PowerBar";
            this.p2PowerBar.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.p2PowerBar.RightToLeftLayout = true;
            this.p2PowerBar.Size = new System.Drawing.Size(184, 27);
            this.p2PowerBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.p2PowerBar.TabIndex = 10;
            this.p2PowerBar.Value = 70;
            // 
            // P2Name
            // 
            this.P2Name.AutoSize = true;
            this.P2Name.BackColor = System.Drawing.Color.Transparent;
            this.P2Name.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.P2Name.Location = new System.Drawing.Point(858, 36);
            this.P2Name.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.P2Name.Name = "P2Name";
            this.P2Name.Size = new System.Drawing.Size(216, 31);
            this.P2Name.TabIndex = 9;
            this.P2Name.Text = "Player 2 Name...";
            // 
            // p2HealthBar
            // 
            this.p2HealthBar.BackColor = System.Drawing.Color.Red;
            this.p2HealthBar.ForeColor = System.Drawing.Color.Green;
            this.p2HealthBar.Location = new System.Drawing.Point(924, 529);
            this.p2HealthBar.Margin = new System.Windows.Forms.Padding(2);
            this.p2HealthBar.Name = "p2HealthBar";
            this.p2HealthBar.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.p2HealthBar.RightToLeftLayout = true;
            this.p2HealthBar.Size = new System.Drawing.Size(184, 27);
            this.p2HealthBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.p2HealthBar.TabIndex = 8;
            this.p2HealthBar.Value = 100;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Blue;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(505, 87);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(114, 50);
            this.button1.TabIndex = 13;
            this.button1.Text = "Attack";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Blue;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(505, 150);
            this.button2.Margin = new System.Windows.Forms.Padding(2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(114, 50);
            this.button2.TabIndex = 14;
            this.button2.Text = "Defend";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.Blue;
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(505, 214);
            this.button3.Margin = new System.Windows.Forms.Padding(2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(114, 50);
            this.button3.TabIndex = 15;
            this.button3.Text = "Powerup";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.Blue;
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Location = new System.Drawing.Point(505, 277);
            this.button4.Margin = new System.Windows.Forms.Padding(2);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(114, 50);
            this.button4.TabIndex = 16;
            this.button4.Text = "Transform";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.Blue;
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Location = new System.Drawing.Point(505, 342);
            this.button5.Margin = new System.Windows.Forms.Padding(2);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(114, 50);
            this.button5.TabIndex = 17;
            this.button5.Text = "ULTI!";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.button6.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.Location = new System.Drawing.Point(491, 407);
            this.button6.Margin = new System.Windows.Forms.Padding(2);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(138, 76);
            this.button6.TabIndex = 18;
            this.button6.Text = "LOCK IN!";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // battledata
            // 
            this.battledata.Location = new System.Drawing.Point(428, 512);
            this.battledata.Margin = new System.Windows.Forms.Padding(2);
            this.battledata.Multiline = true;
            this.battledata.Name = "battledata";
            this.battledata.ReadOnly = true;
            this.battledata.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.battledata.Size = new System.Drawing.Size(257, 97);
            this.battledata.TabIndex = 19;
            // 
            // p1healthbarLabel
            // 
            this.p1healthbarLabel.AutoSize = true;
            this.p1healthbarLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.p1healthbarLabel.Location = new System.Drawing.Point(72, 503);
            this.p1healthbarLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.p1healthbarLabel.Name = "p1healthbarLabel";
            this.p1healthbarLabel.Size = new System.Drawing.Size(36, 20);
            this.p1healthbarLabel.TabIndex = 20;
            this.p1healthbarLabel.Text = "100";
            // 
            // p2healthbarLabel
            // 
            this.p2healthbarLabel.AutoSize = true;
            this.p2healthbarLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.p2healthbarLabel.Location = new System.Drawing.Point(1072, 507);
            this.p2healthbarLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.p2healthbarLabel.Name = "p2healthbarLabel";
            this.p2healthbarLabel.Size = new System.Drawing.Size(36, 20);
            this.p2healthbarLabel.TabIndex = 21;
            this.p2healthbarLabel.Text = "100";
            // 
            // p1powerbarLabel
            // 
            this.p1powerbarLabel.AutoSize = true;
            this.p1powerbarLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.p1powerbarLabel.Location = new System.Drawing.Point(72, 553);
            this.p1powerbarLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.p1powerbarLabel.Name = "p1powerbarLabel";
            this.p1powerbarLabel.Size = new System.Drawing.Size(27, 20);
            this.p1powerbarLabel.TabIndex = 22;
            this.p1powerbarLabel.Text = "70";
            // 
            // p2powerbarLabel
            // 
            this.p2powerbarLabel.AutoSize = true;
            this.p2powerbarLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.p2powerbarLabel.Location = new System.Drawing.Point(1072, 559);
            this.p2powerbarLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.p2powerbarLabel.Name = "p2powerbarLabel";
            this.p2powerbarLabel.Size = new System.Drawing.Size(27, 20);
            this.p2powerbarLabel.TabIndex = 23;
            this.p2powerbarLabel.Text = "70";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(465, 36);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(195, 31);
            this.label5.TabIndex = 24;
            this.label5.Text = "Player 1\'s Turn";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 15000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(458, 5);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(82, 31);
            this.label6.TabIndex = 25;
            this.label6.Text = "15.00";
            // 
            // timer2
            // 
            this.timer2.Enabled = true;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(886, 5);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(106, 24);
            this.button7.TabIndex = 26;
            this.button7.Text = "Pause request";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(544, 5);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(116, 31);
            this.label7.TabIndex = 27;
            this.label7.Text = "Round 1";
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(151, -1);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(106, 24);
            this.button8.TabIndex = 28;
            this.button8.Text = "Pause request";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // timer3
            // 
            this.timer3.Tick += new System.EventHandler(this.timer3_Tick);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(999, 6);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(75, 23);
            this.button9.TabIndex = 29;
            this.button9.Text = "Play VS AI";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(70, 0);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(75, 23);
            this.button10.TabIndex = 30;
            this.button10.Text = "Play VS AI";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // timer4
            // 
            this.timer4.Tick += new System.EventHandler(this.timer4_Tick);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label8.Location = new System.Drawing.Point(325, 30);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(43, 17);
            this.label8.TabIndex = 37;
            this.label8.Text = "WINS";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label9.Location = new System.Drawing.Point(773, 36);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(43, 17);
            this.label9.TabIndex = 38;
            this.label9.Text = "WINS";
            // 
            // Player1RB3
            // 
            this.Player1RB3.AutoSize = true;
            this.Player1RB3.Enabled = false;
            this.Player1RB3.Location = new System.Drawing.Point(354, 3);
            this.Player1RB3.Name = "Player1RB3";
            this.Player1RB3.Size = new System.Drawing.Size(15, 14);
            this.Player1RB3.TabIndex = 39;
            this.Player1RB3.UseVisualStyleBackColor = true;
            // 
            // Player1RB2
            // 
            this.Player1RB2.AutoSize = true;
            this.Player1RB2.Enabled = false;
            this.Player1RB2.Location = new System.Drawing.Point(333, 3);
            this.Player1RB2.Name = "Player1RB2";
            this.Player1RB2.Size = new System.Drawing.Size(15, 14);
            this.Player1RB2.TabIndex = 40;
            this.Player1RB2.UseVisualStyleBackColor = true;
            // 
            // Player1RB1
            // 
            this.Player1RB1.AutoSize = true;
            this.Player1RB1.Enabled = false;
            this.Player1RB1.Location = new System.Drawing.Point(312, 3);
            this.Player1RB1.Name = "Player1RB1";
            this.Player1RB1.Size = new System.Drawing.Size(15, 14);
            this.Player1RB1.TabIndex = 41;
            this.Player1RB1.UseVisualStyleBackColor = true;
            // 
            // Player2RB3
            // 
            this.Player2RB3.AutoSize = true;
            this.Player2RB3.Enabled = false;
            this.Player2RB3.Location = new System.Drawing.Point(776, 11);
            this.Player2RB3.Name = "Player2RB3";
            this.Player2RB3.Size = new System.Drawing.Size(15, 14);
            this.Player2RB3.TabIndex = 44;
            this.Player2RB3.UseVisualStyleBackColor = true;
            // 
            // Player2RB2
            // 
            this.Player2RB2.AutoSize = true;
            this.Player2RB2.Enabled = false;
            this.Player2RB2.Location = new System.Drawing.Point(797, 11);
            this.Player2RB2.Name = "Player2RB2";
            this.Player2RB2.Size = new System.Drawing.Size(15, 14);
            this.Player2RB2.TabIndex = 43;
            this.Player2RB2.UseVisualStyleBackColor = true;
            // 
            // Player2RB1
            // 
            this.Player2RB1.AutoSize = true;
            this.Player2RB1.Enabled = false;
            this.Player2RB1.Location = new System.Drawing.Point(818, 11);
            this.Player2RB1.Name = "Player2RB1";
            this.Player2RB1.Size = new System.Drawing.Size(15, 14);
            this.Player2RB1.TabIndex = 42;
            this.Player2RB1.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(247, 512);
            this.textBox1.Margin = new System.Windows.Forms.Padding(2);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox1.Size = new System.Drawing.Size(156, 97);
            this.textBox1.TabIndex = 47;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(710, 511);
            this.textBox2.Margin = new System.Windows.Forms.Padding(2);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox2.Size = new System.Drawing.Size(161, 97);
            this.textBox2.TabIndex = 48;
            // 
            // fighter1PicBox
            // 
            this.fighter1PicBox.BackColor = System.Drawing.Color.Transparent;
            this.fighter1PicBox.Image = ((System.Drawing.Image)(resources.GetObject("fighter1PicBox.Image")));
            this.fighter1PicBox.Location = new System.Drawing.Point(0, 23);
            this.fighter1PicBox.Name = "fighter1PicBox";
            this.fighter1PicBox.Size = new System.Drawing.Size(468, 596);
            this.fighter1PicBox.TabIndex = 49;
            this.fighter1PicBox.TabStop = false;
            // 
            // fighter2PicBox2
            // 
            this.fighter2PicBox2.Image = global::DBS_GameDraft1_WFA.Properties.Resources.black3Right;
            this.fighter2PicBox2.Location = new System.Drawing.Point(756, 69);
            this.fighter2PicBox2.Margin = new System.Windows.Forms.Padding(2);
            this.fighter2PicBox2.Name = "fighter2PicBox2";
            this.fighter2PicBox2.Size = new System.Drawing.Size(298, 430);
            this.fighter2PicBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.fighter2PicBox2.TabIndex = 7;
            this.fighter2PicBox2.TabStop = false;
            // 
            // fighter1PicBox2
            // 
            this.fighter1PicBox2.Image = global::DBS_GameDraft1_WFA.Properties.Resources.goku3Left;
            this.fighter1PicBox2.Location = new System.Drawing.Point(76, 69);
            this.fighter1PicBox2.Margin = new System.Windows.Forms.Padding(2);
            this.fighter1PicBox2.Name = "fighter1PicBox2";
            this.fighter1PicBox2.Size = new System.Drawing.Size(298, 430);
            this.fighter1PicBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.fighter1PicBox2.TabIndex = 0;
            this.fighter1PicBox2.TabStop = false;
            // 
            // fighter2PicBox
            // 
            this.fighter2PicBox.BackColor = System.Drawing.Color.Transparent;
            this.fighter2PicBox.Image = global::DBS_GameDraft1_WFA.Properties.Resources.black1Right;
            this.fighter2PicBox.Location = new System.Drawing.Point(656, 23);
            this.fighter2PicBox.Name = "fighter2PicBox";
            this.fighter2PicBox.Size = new System.Drawing.Size(468, 596);
            this.fighter2PicBox.TabIndex = 50;
            this.fighter2PicBox.TabStop = false;
            // 
            // Battleground_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1123, 617);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.Player2RB3);
            this.Controls.Add(this.Player2RB2);
            this.Controls.Add(this.Player2RB1);
            this.Controls.Add(this.Player1RB1);
            this.Controls.Add(this.Player1RB2);
            this.Controls.Add(this.Player1RB3);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.p2powerbarLabel);
            this.Controls.Add(this.p1powerbarLabel);
            this.Controls.Add(this.p2healthbarLabel);
            this.Controls.Add(this.p1healthbarLabel);
            this.Controls.Add(this.battledata);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.p2PowerBar);
            this.Controls.Add(this.P2Name);
            this.Controls.Add(this.p2HealthBar);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.p1PowerBar);
            this.Controls.Add(this.P1Name);
            this.Controls.Add(this.p1HealthBar);
            this.Controls.Add(this.fighter1PicBox);
            this.Controls.Add(this.fighter1PicBox2);
            this.Controls.Add(this.fighter2PicBox);
            this.Controls.Add(this.fighter2PicBox2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Battleground_Form";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DB Smack Face";
            this.Load += new System.EventHandler(this.Battleground_Form_Load);
            ((System.ComponentModel.ISupportInitialize)(this.fighter1PicBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fighter2PicBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fighter1PicBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fighter2PicBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox fighter1PicBox2;
        private System.Windows.Forms.ProgressBar p1HealthBar;
        private System.Windows.Forms.Label P1Name;
        private System.Windows.Forms.ProgressBar p1PowerBar;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ProgressBar p2PowerBar;
        private System.Windows.Forms.Label P2Name;
        private System.Windows.Forms.ProgressBar p2HealthBar;
        private System.Windows.Forms.PictureBox fighter2PicBox2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.TextBox battledata;
        private System.Windows.Forms.Label p1healthbarLabel;
        private System.Windows.Forms.Label p2healthbarLabel;
        private System.Windows.Forms.Label p1powerbarLabel;
        private System.Windows.Forms.Label p2powerbarLabel;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Timer timer3;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Timer timer4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.CheckBox Player1RB3;
        private System.Windows.Forms.CheckBox Player1RB2;
        private System.Windows.Forms.CheckBox Player1RB1;
        private System.Windows.Forms.CheckBox Player2RB3;
        private System.Windows.Forms.CheckBox Player2RB2;
        private System.Windows.Forms.CheckBox Player2RB1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.PictureBox fighter1PicBox;
        private System.Windows.Forms.PictureBox fighter2PicBox;
    }
}