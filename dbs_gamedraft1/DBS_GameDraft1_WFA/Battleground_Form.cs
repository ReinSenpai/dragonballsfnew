﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Media;
using System.Reflection;

namespace DBS_GameDraft1_WFA
{
    public partial class Battleground_Form : Form
    {
        public bool P1attackButtonClicked = false;
        public bool P2attackButtonClicked = false;
        public bool P1blockButtonClicked = false;
        public bool P2blockButtonClicked = false;
        public bool P1powerupButtonClicked = false;
        public bool P2powerupButtonClicked = false;
        public bool P1transformButtonClicked = false;
        public bool P2transformButtonClicked = false;
        public bool P1ultimateButtonClicked = false;
        public bool P2ultimateButtonClicked = false;
        public bool P1Skip = false;
        public bool P2Skip = false;
        public bool p2itemnotselectedButtonClicked = false;

        int roundNumber = 1;
        Random rnd = new Random();
        Random rndm = new Random();
        Random rndom = new Random();
        Random zeno = new Random();
        Random AIrandom = new Random();
        double time = 15;
        int ran = 0;
        int ran2 = 0;
        int rand = 0;
        int zenowin = 0;
        int clicks2 = 0;
        int RoundCount = 1;
        private Fighters P1;
        private Fighters P2;
        private List<Fighters> Fighter = new List<Fighters>();

        public void P1resetround() // PLAYER 1 RESTART FUNCTION
        {
            P1attackButtonClicked = false;
            P1blockButtonClicked = false;
            P1powerupButtonClicked = false;
            P1transformButtonClicked = false;
            P1ultimateButtonClicked = false;
            P1.PassiveDodge = false;
            P1.PassiveDoubleDamage = false;
            P1.PassiveHalfDamage = false;
            P1.PassiveTriplePowerup = false;
            P1.PassiveSteal = false;
            P1.PassiveAbsorb = false;
            P1.PassiveSkip = false;
        }
        public void P2resetround() // PLAYER 2 RESTART FUNCTION
        {
            P2attackButtonClicked = false;
            P2blockButtonClicked = false;
            P2powerupButtonClicked = false;
            P2transformButtonClicked = false;
            P2ultimateButtonClicked = false;
            p2itemnotselectedButtonClicked = false;
            P2.PassiveDodge = false;
            P2.PassiveDoubleDamage = false;
            P2.PassiveHalfDamage = false;
            P2.PassiveTriplePowerup = false;
            P2.PassiveSteal = false;
            P2.PassiveAbsorb = false;
            P1.PassiveSkip = false;
        }
        public void resetButtons() // CAHNGES BUTTON COLORS BACK TO BLUE
        {
            button1.BackColor = Color.Blue;
            button2.BackColor = Color.Blue;
            button3.BackColor = Color.Blue;
            button4.BackColor = Color.Blue;
            button5.BackColor = Color.Blue;
        }
        public Battleground_Form(Fighters P1, Fighters P2) // We pass the Two Players into this form
        {
            InitializeComponent();

            SoundPlayer audio = new SoundPlayer(DBS_GameDraft1_WFA.Properties.Resources.Ultimate_Battle___Dragon_ball_Super_Instrumental_);
            audio.PlayLooping();

            // We set the two players
            this.P1 = P1;
            this.P2 = P2;

        }
        public void button1_Click(object sender, EventArgs e) // ATTACK BUTTON
        {
            if (clicks2 == 0)
            {
                AttackValidations(P1, P1attackButtonClicked, P1blockButtonClicked, P2powerupButtonClicked, P1transformButtonClicked, P1ultimateButtonClicked, p1PowerBar, button1, out P1attackButtonClicked);
            }
            else
            {
                AttackValidations(P2, P2attackButtonClicked, P2blockButtonClicked, P2powerupButtonClicked, P2transformButtonClicked, P2ultimateButtonClicked, p2PowerBar, button1, out P2attackButtonClicked);
            }
        }
        public void button2_Click(object sender, EventArgs e)// BLOCK BUTTON
        {
            if (clicks2 == 0)
            {
                BlockValidations(P1, P1attackButtonClicked, P1blockButtonClicked, P2powerupButtonClicked, P1transformButtonClicked, P1ultimateButtonClicked, p1PowerBar, button2, out P1blockButtonClicked);
            }
            else
            {
                BlockValidations(P2, P2attackButtonClicked, P2blockButtonClicked, P2powerupButtonClicked, P2transformButtonClicked, P2ultimateButtonClicked, p2PowerBar, button2, out P2blockButtonClicked);
            }
        }
        public void button3_Click(object sender, EventArgs e)// POWER UP BUTTON
        {
            if (clicks2 == 0)
            {
                PowerUpValidations(P1, P1attackButtonClicked, P1blockButtonClicked, P1powerupButtonClicked, P1transformButtonClicked, P1ultimateButtonClicked, p1PowerBar, button3, out P1powerupButtonClicked);
            }
            else
            {
                PowerUpValidations(P2, P2attackButtonClicked, P2blockButtonClicked, P2powerupButtonClicked, P2transformButtonClicked, P2ultimateButtonClicked, p2PowerBar, button3, out P2powerupButtonClicked);
            }
        }
        public void button4_Click(object sender, EventArgs e)// TRANSFORM BUTTON
        {
            if (clicks2 == 0)
            {
                TransformValidations(P1, P1attackButtonClicked, P1blockButtonClicked, P1powerupButtonClicked, P1transformButtonClicked, P1ultimateButtonClicked, p1PowerBar, button4, out P1transformButtonClicked);
            }
            else
            {
                TransformValidations(P2, P2attackButtonClicked, P2blockButtonClicked, P2powerupButtonClicked, P2transformButtonClicked, P2ultimateButtonClicked, p2PowerBar, button4, out P2transformButtonClicked);
            }
        }
        public void button5_Click(object sender, EventArgs e) //ULT BUTTON
        {
            if (clicks2 == 0)
            {
                UltimateValidations(P1, P1attackButtonClicked, P1blockButtonClicked, P1powerupButtonClicked, P1transformButtonClicked, P1ultimateButtonClicked, p1HealthBar, p1PowerBar, button5, out P1ultimateButtonClicked);
            }
            else
            {
                UltimateValidations(P2, P2attackButtonClicked, P2blockButtonClicked, P2powerupButtonClicked, P2transformButtonClicked, P2ultimateButtonClicked, p2HealthBar, p2PowerBar, button5, out P2ultimateButtonClicked);
            }
        }
        public void button6_Click(object sender, EventArgs e) //LOCK IN BUTTON
        {
            if (clicks2 == 0)
            {
                LockIn();
            }
            else
            {
                LockIn();
            }
        }
        public void AttackValidations(Fighters CurrentPlayer, bool Attacked, bool Blocked, bool Powerup, bool Transformed, bool Ult, ProgressBar PowerBar, Button AttackButton, out bool Attacked2) // ATTACK METHOD
        {
            Attacked2 = false;
            if (Blocked == false & Powerup == false & Transformed == false & Ult == false)
            {
                if (CurrentPlayer.PowerCost <= PowerBar.Value)
                {
                    if (Attacked == true)
                    {
                        AttackButton.BackColor = Color.Blue;
                        Attacked2 = false;
                    }
                    else
                    {
                        AttackButton.BackColor = Color.Violet;
                        Attacked2 = true;
                    }
                }
                else
                {
                    MessageBox.Show("Not enough power to attack!");
                }
            }
            else
            {
                MessageBox.Show("You can only select one action");
            }
        }
        public void BlockValidations(Fighters CurrentPlayer, bool Attacked, bool Blocked, bool Powerup, bool Transformed, bool Ult, ProgressBar PowerBar, Button BlockButton, out bool Blocked2) // BLOCK METHOD
        {
            Blocked2 = false;

            if (Attacked == false & Powerup == false & Transformed == false & Ult == false)
            {
                if (Blocked == true)
                {
                    BlockButton.BackColor = Color.Blue;
                    Blocked2 = false;
                }
                else
                {
                    BlockButton.BackColor = Color.Violet;
                    Blocked2 = true;
                }
            }
            else
            {
                MessageBox.Show("You can only select one action");
            }
        }
        public void PowerUpValidations(Fighters CurrentPlayer, bool Attacked, bool Blocked, bool Powerup, bool Transformed, bool Ult, ProgressBar PowerBar, Button PowerupButton, out bool Powerup2) // POWERUP METHOD
        {
            Powerup2 = false;

            if (Blocked == false & Attacked == false & Transformed == false & Ult == false)
            {
                if (PowerBar.Value != 100)
                {
                    if (Powerup == true)
                    {
                        PowerupButton.BackColor = Color.Blue;
                        Powerup2 = false;
                    }
                    else
                    {
                        PowerupButton.BackColor = Color.Violet;
                        Powerup2 = true;
                    }
                }
                else
                {
                    MessageBox.Show("You are already full power");
                }
            }
            else
            {
                MessageBox.Show("You can only select one action");
            }
        }
        public void TransformValidations(Fighters CurrentPlayer, bool Attacked, bool Blocked, bool Powerup, bool Transformed, bool Ult, ProgressBar PowerBar, Button TransformButton, out bool Transformed2) // TRANSFORM METHOD
        {
            Transformed2 = false;
            if (Blocked == false & Attacked == false & Powerup == false & Ult == false)
            {
                if (CurrentPlayer.Upgradable == true && PowerBar.Value >= CurrentPlayer.TransformCost)
                {
                    if (Transformed == true)
                    {
                        TransformButton.BackColor = Color.Blue;
                        Transformed2 = false;
                    }
                    else
                    {
                        TransformButton.BackColor = Color.Violet;
                        Transformed2 = true;
                    }
                }
                else
                {
                    MessageBox.Show("You are unable to transform");
                }
            }
            else
            {
                MessageBox.Show("You can only select one action");
            }
        }
        public void UltimateValidations(Fighters CurrentPlayer, bool Attacked, bool Blocked, bool Powerup, bool Transformed, bool Ult, ProgressBar HealthBar, ProgressBar PowerBar, Button UltButton, out bool Ult2) // ULTIMATE METHOD
        {
            Ult2 = false;
            if (Attacked == false & Blocked == false & Powerup == false & Transformed == false)
            {
                if (CurrentPlayer.CanUlt == true && CurrentPlayer.UltCost <= PowerBar.Value & roundNumber >= 4)
                {
                    if (Ult == true)
                    {
                        UltButton.BackColor = Color.Blue;
                        Ult2 = false;
                    }
                    else
                    {
                        if (CurrentPlayer.Name == "Gohan" & HealthBar.Value > CurrentPlayer.Heal)
                        {
                            MessageBox.Show("You need to be below " + CurrentPlayer.Heal + "% health to use your ultimate");
                            return;
                        }
                        if (CurrentPlayer.Name == "Goku" & HealthBar.Value > 50)
                        {
                            MessageBox.Show("You need to be below 50% health to use your ultimate");
                            return;
                        }
                        UltButton.BackColor = Color.Violet;
                        Ult2 = true;
                    }
                }
                else
                {
                    if (roundNumber < 4)
                    {
                        MessageBox.Show("Cannot ult before round 4");
                    }
                    else
                    {
                        if (CurrentPlayer.CanUlt == false)
                        {
                            MessageBox.Show(CurrentPlayer.Name + " cannot ult in their current form");
                        }
                        else
                        {
                            if (CurrentPlayer.UltCost > PowerBar.Value)
                            {
                                MessageBox.Show("Not enough power for ultimate!");
                            }
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("You can only select one action");
            }
        }
        public void LockIn() // LOCK IN METHOD
        {
            if (clicks2 == 0)
            {
                if (P1Skip == true)
                {
                    MessageBox.Show("You're stuck in time skip");
                    label5.Text = "Player 2's Turn";
                    clicks2 = 1;
                    timer1.Stop();
                    timer1.Start();
                    resetButtons();
                    time = 15;
                    P1Skip = false;
                    return;
                }

                if (P1attackButtonClicked == false && P1blockButtonClicked == false && P1powerupButtonClicked == false && P1transformButtonClicked == false && P1ultimateButtonClicked == false)
                {
                    MessageBox.Show("You need to select an action!");
                }
                else
                {
                    PassiveMethod(P1, p1PowerBar, p1HealthBar, p1powerbarLabel, p1healthbarLabel);
                    battledata.SelectionStart = battledata.Text.Length;
                    battledata.ScrollToCaret();
                    label5.Text = "Player 2's Turn";
                    clicks2 = 1;
                    timer1.Stop();
                    timer1.Start();
                    resetButtons();
                    time = 15;
                    return;
                }
            }
            if (clicks2 == 1)
            {
                if (P2Skip == true)
                {
                    MessageBox.Show("You're stuck in time skip");
                    p2itemnotselectedButtonClicked = true;
                    P2Skip = false;
                    P2attackButtonClicked = false;
                    P2blockButtonClicked = false;
                    P2powerupButtonClicked = false;
                    P2transformButtonClicked = false;
                    P2ultimateButtonClicked = false;
                }
                if (P2attackButtonClicked == false && P2blockButtonClicked == false && P2powerupButtonClicked == false && P2transformButtonClicked == false && P2ultimateButtonClicked == false && p2itemnotselectedButtonClicked == false)
                {
                    MessageBox.Show("You need to select an action!");
                }
                else
                {
                    PassiveMethod(P2, p2PowerBar, p2HealthBar, p2powerbarLabel, p2healthbarLabel);
                    battledata.SelectionStart = battledata.Text.Length;
                    battledata.ScrollToCaret();

                    if (P1attackButtonClicked == true)
                    {
                        AttackMethod(P1, P2, P2blockButtonClicked, p1PowerBar, p2HealthBar, p2PowerBar, p1powerbarLabel, p2healthbarLabel, p2powerbarLabel);
                    }
                    if (P2attackButtonClicked == true)
                    {
                        AttackMethod(P2, P1, P1blockButtonClicked, p2PowerBar, p1HealthBar, p1PowerBar, p2powerbarLabel, p1healthbarLabel, p1powerbarLabel);
                    }
                    if (P1powerupButtonClicked == true)
                    {
                        PowerUpMethod(P1, p1PowerBar, p1powerbarLabel);
                    }
                    if (P2powerupButtonClicked == true)
                    {
                        PowerUpMethod(P2, p2PowerBar, p2powerbarLabel);
                    }
                    if (P1ultimateButtonClicked == true)
                    {
                        UltimateMethod(P1, P2, p1powerbarLabel, p2healthbarLabel, p1healthbarLabel, p1PowerBar, p2HealthBar, p1HealthBar, P2blockButtonClicked, fighter1PicBox);
                    }
                    if (P2ultimateButtonClicked == true)
                    {
                        UltimateMethod(P2, P1, p2powerbarLabel, p1healthbarLabel, p2healthbarLabel, p2PowerBar, p1HealthBar, p2HealthBar, P1blockButtonClicked, fighter2PicBox);
                    }
                    if (P1transformButtonClicked == true)
                    {
                        TransformMethod(P1, fighter1PicBox, p1PowerBar, p1powerbarLabel);
                    }
                    if (P2transformButtonClicked == true)
                    {
                        TransformMethod(P2, fighter2PicBox, p2PowerBar, p2powerbarLabel);
                    }

                    CurrentCharacterStats(P1, textBox1);
                    CurrentCharacterStats(P2, textBox2);
                    clicks2 = 0;
                    label5.Text = "Player 1's Turn";
                    roundNumber++;
                    label7.Text = "Round " + roundNumber;
                    P1resetround();
                    P2resetround();
                    timer1.Stop();
                    timer1.Start();
                    resetButtons();
                    time = 15;

                    wincheck();

                    battledata.SelectionStart = battledata.Text.Length;
                    battledata.ScrollToCaret();
                }
            }
        }
        private void Battleground_Form_Load(object sender, EventArgs e) //FORM LOAD CHARACTER PORTAITS
        {
            fighter1PicBox.Image = P1.PortraitLeft;
            fighter2PicBox.Image = P2.PortraitRight;

            CurrentCharacterStats(P1, textBox1);
            CurrentCharacterStats(P2, textBox2);

            // We change the label text to the player names
            P1Name.Text = P1.Name;
            P2Name.Text = P2.Name;
            roundNumber = 1;
            label7.Text = "Round " + roundNumber;

            zenowin = zeno.Next(1, 5);

            if (Player1RB2.Checked == true & Player2RB2.Checked == true)
            {
                battledata.Text += "                               Final Round \r\n";
            }
            else
            {
                battledata.Text += "                                Round " + RoundCount + "\r\n";
            }
            battledata.Text += "_______________________________________\r\n";

            timer1.Enabled = true;
            timer2.Enabled = true;
            timer3.Enabled = true;
            timer4.Enabled = true;

            if (zenowin == 1)
            {
                if (P1.Name == "Zeno")
                {
                    Player1RB1.Checked = true;
                    Player1RB2.Checked = true;
                    Player1RB3.Checked = true;
                    battledata.Text += "Zeno erased " + P2.Name + " from the game";
                    wincheck();
                }
                if (P2.Name == "Zeno")
                {
                    Player2RB1.Checked = true;
                    Player2RB2.Checked = true;
                    Player2RB3.Checked = true;
                    battledata.Text += "Zeno erased " + P2.Name + " from the game";
                    wincheck();
                }
            }
        }
        private void timer1_Tick(object sender, EventArgs e) // 15 SECOND MAX TIMER
        {
            if (clicks2 == 0)
            {
                P1resetround();
                label5.Text = "Player 2's Turn";
                clicks2 = 1;
                timer1.Stop();
                timer1.Start();
                time = 15;
                resetButtons();
                return;
            }
            if (clicks2 == 1)
            {
                P2resetround();
                p2itemnotselectedButtonClicked = true;
                LockIn();
            }
        }
        private void timer2_Tick(object sender, EventArgs e) // ROUND COUNT DOWN 
        {
            time -= 0.1;
            string time2 = time.ToString("0.00");
            label6.Text = time2;
        }
        private void button9_Click(object sender, EventArgs e) // START PLAYER 2
        {
            P2Name.Text = "AI " + P2.Name;
            timer4.Enabled = true;
        }
        private void button10_Click(object sender, EventArgs e) // START PLAYER 1
        {
            P1Name.Text = "AI " + P1.Name;
            timer3.Enabled = true;
        }
        private void timer3_Tick(object sender, EventArgs e) //pretend thinking time random amounts wow 
        {
            timer3.Interval = rnd.Next(1250, 2350);
            ran = rndom.Next(1, 3);

            if (clicks2 == 0)
            {
                AI(P1, P2, p1HealthBar, p2HealthBar, p1PowerBar, p2PowerBar, ref P1attackButtonClicked, ref P1blockButtonClicked, ref P1powerupButtonClicked, ref P1transformButtonClicked, ref P1ultimateButtonClicked);
            }
        }
        private void timer4_Tick(object sender, EventArgs e) //pretend thinking time random amounts wow 
        {
            timer4.Interval = rnd.Next(1250, 2350);
            rand = rndm.Next(1, 3);

            if (clicks2 == 1)
            {
                AI(P2, P1, p2HealthBar, p1HealthBar, p2PowerBar, p1PowerBar, ref P2attackButtonClicked, ref P2blockButtonClicked, ref P2powerupButtonClicked, ref P2transformButtonClicked, ref P2ultimateButtonClicked);
            }
        }
        public void TransformMethod(Fighters CurrentPlayer, PictureBox NewPicture, ProgressBar PowerBar, Label PowerLabel)
        {
            if (CurrentPlayer.Name == "Goku")
            {
                if (CurrentPlayer.Form == 3)
                {
                    CurrentPlayer.Upgradable = false;
                    CurrentPlayer.CanUlt = true;
                    CurrentPlayer.PassiveChance = 40;
                    battledata.Text += "Goku has transformed into ultra instinct \r\n";
                    NewPicture.Image = Properties.Resources.goku5Right;
                    if (CurrentPlayer == P1)
                    {
                        NewPicture.Image = Properties.Resources.goku5Left;
                    }
                }
                if (CurrentPlayer.Form == 2)
                {
                    battledata.Text += "Goku has transformed into super saiyan blue \r\n";
                    NewPicture.Image = Properties.Resources.goku4Right;
                    if (CurrentPlayer == P1)
                    {
                        NewPicture.Image = Properties.Resources.Goku4Left;
                    }
                }
                if (CurrentPlayer.Form == 1)
                {
                    battledata.Text += "Goku has transformed into super saiyan god \r\n";
                    NewPicture.Image = Properties.Resources.goku3Right;
                    if (CurrentPlayer == P1)
                    {
                        NewPicture.Image = Properties.Resources.goku3Left;
                    }
                }
                if (CurrentPlayer.Form == 0)
                {
                    battledata.Text += "Goku has transformed into super saiyan 2 \r\n";
                    NewPicture.Image = Properties.Resources.goku2Right;
                    if (CurrentPlayer == P1)
                    {
                        NewPicture.Image = Properties.Resources.Goku2Left;
                    }
                }
                CurrentPlayer.Damage += 5;
                PowerBar.Value = PowerBar.Value - CurrentPlayer.TransformCost;
                PowerLabel.Text = PowerBar.Value.ToString();
            }
            if (CurrentPlayer.Name == "Vegeta")
            {
                if (CurrentPlayer.Form == 3)
                {
                    CurrentPlayer.Upgradable = false;
                    CurrentPlayer.PassiveChance = 20;
                    battledata.Text += "Vegeta has transformed into ultra instinct \r\n";
                    NewPicture.Image = Properties.Resources.vegeta5Right;
                    if (CurrentPlayer == P1)
                    {
                        NewPicture.Image = Properties.Resources.Vegeta5Left;
                    }
                }
                if (CurrentPlayer.Form == 2)
                {
                    battledata.Text += "Vegeta has transformed into super saiyan blue \r\n";
                    NewPicture.Image = Properties.Resources.vegeta4Right;
                    if (CurrentPlayer == P1)
                    {
                        NewPicture.Image = Properties.Resources.vegeta4Left;
                    }
                }
                if (CurrentPlayer.Form == 1)
                {
                    battledata.Text += "Vegeta has transformed into super saiyan 2 \r\n";
                    NewPicture.Image = Properties.Resources.vegeta3Right;
                    if (CurrentPlayer == P1)
                    {
                        NewPicture.Image = Properties.Resources.vegeta3Left;
                    }
                }
                if (CurrentPlayer.Form == 0)
                {
                    battledata.Text += "Vegeta has transformed into super saiyan 1 \r\n";
                    NewPicture.Image = Properties.Resources.vegeta2Right;
                    if (CurrentPlayer == P1)
                    {
                        NewPicture.Image = Properties.Resources.vegeta2Left;
                    }
                }
                CurrentPlayer.Damage += 5;
                CurrentPlayer.PassiveChance += 5;
                PowerBar.Value = PowerBar.Value - CurrentPlayer.TransformCost;
                PowerLabel.Text = PowerBar.Value.ToString();
            }
            if (CurrentPlayer.Name == "Vegito")
            {
                if (CurrentPlayer.Form == 1)
                {
                    CurrentPlayer.Upgradable = false;
                    CurrentPlayer.CanUlt = true;
                    CurrentPlayer.PassiveChance = 70;
                    CurrentPlayer.PassiveUnblockable = true;
                    battledata.Text += "Vegito has transformed into super saiyan blue \r\n";
                    NewPicture.Image = Properties.Resources.vegito3Right;
                    if (CurrentPlayer == P1)
                    {
                        NewPicture.Image = Properties.Resources.vegito3Left;
                    }
                }
                if (CurrentPlayer.Form == 0)
                {
                    CurrentPlayer.PassiveChance = 50;
                    battledata.Text += "Vegito has transformed into super saiyan 2 \r\n";
                    NewPicture.Image = Properties.Resources.vegito2Right;
                    if (CurrentPlayer == P1)
                    {
                        NewPicture.Image = Properties.Resources.vegito2Left;
                    }
                }
                CurrentPlayer.Damage += 10;
                PowerBar.Value = PowerBar.Value - CurrentPlayer.TransformCost;
                PowerLabel.Text = PowerBar.Value.ToString();
            }
            if (CurrentPlayer.Name == "Gohan")
            {
                if (CurrentPlayer.Form == 2)
                {
                    CurrentPlayer.Upgradable = false;
                    CurrentPlayer.Ult = 100;
                    CurrentPlayer.Heal = 100;
                    battledata.Text += "Gohan has transformed into Mystic Gohan \r\n";
                    NewPicture.Image = Properties.Resources.gohan4Right;
                    if (CurrentPlayer == P1)
                    {
                        NewPicture.Image = Properties.Resources.gohan4Left;
                    }
                }
                if (CurrentPlayer.Form == 1)
                {
                    CurrentPlayer.CanUlt = true;
                    battledata.Text += "Gohan has transformed into super saiyan 2 \r\n";
                    NewPicture.Image = Properties.Resources.gohan3Right;
                    if (CurrentPlayer == P1)
                    {
                        NewPicture.Image = Properties.Resources.gohan3Left;
                    }
                }
                if (CurrentPlayer.Form == 0)
                {
                    battledata.Text += "Gohan has transformed into super saiyan 1 \r\n";
                    NewPicture.Image = Properties.Resources.gohan2Right;
                    if (CurrentPlayer == P1)
                    {
                        NewPicture.Image = Properties.Resources.gohan2Left;
                    }
                }
                CurrentPlayer.Damage += 5;
                PowerBar.Value = PowerBar.Value - CurrentPlayer.TransformCost;
                PowerLabel.Text = PowerBar.Value.ToString();
            }
            if (CurrentPlayer.Name == "Beerus")
            {
                CurrentPlayer.Damage += 10;
                CurrentPlayer.Upgradable = false;
                CurrentPlayer.CanUlt = true;
                PowerBar.Value = PowerBar.Value - CurrentPlayer.TransformCost;
                PowerLabel.Text = PowerBar.Value.ToString();
                battledata.Text += "Beerus has reached his final form \r\n";
                NewPicture.Image = Properties.Resources.beerus2Right;
                if (CurrentPlayer == P1)
                {
                    NewPicture.Image = Properties.Resources.beerus2Left;
                }
            }
            if (CurrentPlayer.Name == "Whis")
            {
                CurrentPlayer.Damage += 15;
                CurrentPlayer.Upgradable = false;
                CurrentPlayer.CanUlt = true;
                CurrentPlayer.PassiveUnblockable = true;
                PowerBar.Value = PowerBar.Value - CurrentPlayer.TransformCost;
                PowerLabel.Text = PowerBar.Value.ToString();
                CurrentPlayer.PassiveChance = 90;
                battledata.Text += "Whis has reached his final form \r\n";
                NewPicture.Image = Properties.Resources.whis2Right;
                if (CurrentPlayer == P1)
                {
                    NewPicture.Image = Properties.Resources.whis2Left;
                }
            }
            if (CurrentPlayer.Name == "Jiren")
            {
                CurrentPlayer.Upgradable = false;
                CurrentPlayer.Heal = 40;
                CurrentPlayer.UltCost = 75;
                CurrentPlayer.Ult = 70;
                CurrentPlayer.Damage += 10;
                PowerBar.Value = PowerBar.Value - CurrentPlayer.TransformCost;
                PowerLabel.Text = PowerBar.Value.ToString();
                CurrentPlayer.PassiveChance = 30;
                battledata.Text += "Jiren has reached his final form \r\n";
                NewPicture.Image = Properties.Resources.jiren2Right;
                if (CurrentPlayer == P1)
                {
                    NewPicture.Image = Properties.Resources.jiren2Left;
                }
            }
            if (CurrentPlayer.Name == "Sasuke")
            {
                CurrentPlayer.Damage += 5;
                CurrentPlayer.PassiveChance = 20;
                CurrentPlayer.Upgradable = false;
                CurrentPlayer.CanUlt = true;
                PowerBar.Value = PowerBar.Value - CurrentPlayer.TransformCost;
                PowerLabel.Text = PowerBar.Value.ToString();
                battledata.Text += "Sasuke activated his sharingan and rinnegan \r\n";
                NewPicture.Image = Properties.Resources.sasuke2Left2;
                if (CurrentPlayer == P1)
                {
                    NewPicture.Image = Properties.Resources.sasuke2Left;
                }
            }
            if (CurrentPlayer.Name == "Frieza")
            {
                CurrentPlayer.Damage += 15;
                CurrentPlayer.Ult += 15;
                CurrentPlayer.Upgradable = false;
                CurrentPlayer.CanUlt = true;
                PowerBar.Value = PowerBar.Value - CurrentPlayer.TransformCost;
                PowerLabel.Text = PowerBar.Value.ToString();
                battledata.Text += "Frieza transformed into golden Frieza \r\n";
                NewPicture.Image = Properties.Resources.frieza2Right;
                if (CurrentPlayer == P1)
                {
                    NewPicture.Image = Properties.Resources.frieza2Left;
                }
            }
            if (CurrentPlayer.Name == "Android 17")
            {
                CurrentPlayer.Damage += 10;
                CurrentPlayer.Upgradable = false;
                CurrentPlayer.CanUlt = true;
                CurrentPlayer.PassiveChance = 25;
                CurrentPlayer.PowerCost = 10;
                CurrentPlayer.Heal = 15;
                PowerBar.Value = PowerBar.Value - CurrentPlayer.TransformCost;
                PowerLabel.Text = PowerBar.Value.ToString();
                battledata.Text += "Android 17 has fused to become super 17 \r\n";
                NewPicture.Image = Properties.Resources.android2Right;
                if (CurrentPlayer == P1)
                {
                    NewPicture.Image = Properties.Resources.android2Left;
                }
            }
            if (CurrentPlayer.Name == "Goku Black")
            {
                if (CurrentPlayer.Form == 1)
                {
                    CurrentPlayer.Upgradable = false;
                    CurrentPlayer.CanUlt = true;
                    CurrentPlayer.Damage = 35;
                    battledata.Text += "Goku black has transformed into super saiyan rose \r\n";
                    NewPicture.Image = Properties.Resources.black3Right;
                    if (CurrentPlayer == P1)
                    {
                        NewPicture.Image = Properties.Resources.black3Left;
                    }
                }
                if (CurrentPlayer.Form == 0)
                {
                    CurrentPlayer.Damage = 25;
                    battledata.Text += "goku black has transformed into super saiyan 2 \r\n";
                    NewPicture.Image = Properties.Resources.black2Right;
                    if (CurrentPlayer == P1)
                    {
                        NewPicture.Image = Properties.Resources.black2Left;
                    }
                }
                PowerBar.Value = PowerBar.Value - CurrentPlayer.TransformCost;
                PowerLabel.Text = PowerBar.Value.ToString();
            }
            if (CurrentPlayer.Name == "Zamasu")
            {
                CurrentPlayer.PassiveChance = 50;
                CurrentPlayer.Damage += 15;
                CurrentPlayer.Upgradable = false;
                CurrentPlayer.CanUlt = true;
                PowerBar.Value = PowerBar.Value - CurrentPlayer.TransformCost;
                PowerLabel.Text = PowerBar.Value.ToString();
                battledata.Text += "Zamasu fused with Goku Black \r\n";
                NewPicture.Image = Properties.Resources.zamasu2Right;
                if (CurrentPlayer == P1)
                {
                    NewPicture.Image = Properties.Resources.zamasu2Left;
                }
            }
            if (CurrentPlayer.Name == "Hit")
            {
                CurrentPlayer.Ult += 20;
                CurrentPlayer.PassiveChance = 65;
                CurrentPlayer.Upgradable = false;
                CurrentPlayer.CanUlt = true;
                PowerBar.Value = PowerBar.Value - CurrentPlayer.TransformCost;
                PowerLabel.Text = PowerBar.Value.ToString();
                battledata.Text += "Hit reached his final form \r\n";
                NewPicture.Image = Properties.Resources.hit2Right;
                if (CurrentPlayer == P1)
                {
                    NewPicture.Image = Properties.Resources.hit2Left;
                }
            }
            if (CurrentPlayer.Name == "Kefla")
            {
                CurrentPlayer.Damage += 15;
                CurrentPlayer.Upgradable = false;
                CurrentPlayer.CanUlt = true;
                PowerBar.Value = PowerBar.Value - CurrentPlayer.TransformCost;
                PowerLabel.Text = PowerBar.Value.ToString();
                battledata.Text += "Kefla has transformed into super saiyan 2\r\n";
                NewPicture.Image = Properties.Resources.kefla2Right;
                if (CurrentPlayer == P1)
                {
                    NewPicture.Image = Properties.Resources.kefla2Left;
                }
            }
            if (CurrentPlayer.Name == "Toppo")
            {
                CurrentPlayer.Damage += 15;
                CurrentPlayer.PassiveChance = 35;
                CurrentPlayer.Upgradable = false;
                CurrentPlayer.CanUlt = true;
                PowerBar.Value = PowerBar.Value - CurrentPlayer.TransformCost;
                PowerLabel.Text = PowerBar.Value.ToString();
                battledata.Text += "Toppo has reached his final form\r\n";
                NewPicture.Image = Properties.Resources.toppo2Left;
            }
            if (CurrentPlayer.Name == "Trunks")
            {
                if (CurrentPlayer.Form == 1)
                {
                    CurrentPlayer.Upgradable = false;
                    CurrentPlayer.CanUlt = true;
                    CurrentPlayer.Damage = 35;
                    battledata.Text += "Trunks has reached full power \r\n";
                    NewPicture.Image = Properties.Resources.trunks3Right;
                    if (CurrentPlayer == P1)
                    {
                        NewPicture.Image = Properties.Resources.trunks3Left;
                    }
                }
                if (CurrentPlayer.Form == 0)
                {
                    CurrentPlayer.Damage += 25;
                    battledata.Text += "Trunks has transformed into super saiyan 2 \r\n";
                    NewPicture.Image = Properties.Resources.trunks2Right;
                    if (CurrentPlayer == P1)
                    {
                        NewPicture.Image = Properties.Resources.Trunks2Left;
                    }
                }
                PowerBar.Value = PowerBar.Value - CurrentPlayer.TransformCost;
                PowerLabel.Text = PowerBar.Value.ToString();
            }
            if (CurrentPlayer.Name == "Naruto")
            {
                if (CurrentPlayer.Form == 1)
                {
                    CurrentPlayer.CanUlt = true;
                    CurrentPlayer.Upgradable = false;
                    CurrentPlayer.PowerCost = 15;
                    battledata.Text += "Naruto unlocked the six paths \r\n";
                    NewPicture.Image = Properties.Resources.naruto3Right;
                    if (CurrentPlayer == P1)
                    {
                        NewPicture.Image = Properties.Resources.naruto3Left;
                    }
                }
                if (CurrentPlayer.Form == 0)
                {
                    CurrentPlayer.TransformCost = 30;
                    battledata.Text += "Naruto went into sage mode \r\n";
                    NewPicture.Image = Properties.Resources.naruto2Right;
                    if (CurrentPlayer == P1)
                    {
                        NewPicture.Image = Properties.Resources.naruto2Left;
                    }
                }
                CurrentPlayer.Damage += 5;
                CurrentPlayer.PassiveChance += 5;
                PowerBar.Value = PowerBar.Value - CurrentPlayer.TransformCost;
                PowerLabel.Text = PowerBar.Value.ToString();
            }
            if (CurrentPlayer.Name == "Madara")
            {
                if (CurrentPlayer.Form == 1)
                {
                    CurrentPlayer.Ult = 80;
                    CurrentPlayer.UltCost = 70;
                    CurrentPlayer.Upgradable = false;
                    CurrentPlayer.PassiveChance = 35;
                    CurrentPlayer.PowerCost = 15;
                    battledata.Text += "Madara unlocked the six paths \r\n";
                    NewPicture.Image = Properties.Resources.madara3Right;
                    if (CurrentPlayer == P1)
                    {
                        NewPicture.Image = Properties.Resources.madara3Left;
                    }
                }
                if (CurrentPlayer.Form == 0)
                {
                    CurrentPlayer.TransformCost = 20;
                    CurrentPlayer.PassiveChance = 30;
                    CurrentPlayer.CanUlt = true;
                    battledata.Text += "Madara gained sasunoo \r\n";
                    NewPicture.Image = Properties.Resources.madara2Right;
                    if (CurrentPlayer == P1)
                    {
                        NewPicture.Image = Properties.Resources.madara2Left;
                    }
                }
                CurrentPlayer.Damage += 5;
                PowerBar.Value = PowerBar.Value - CurrentPlayer.TransformCost;
                PowerLabel.Text = PowerBar.Value.ToString();
            }
            if (CurrentPlayer.Name == "Jin")
            {
                CurrentPlayer.CanUlt = true;
                CurrentPlayer.Upgradable = false;
                CurrentPlayer.Damage = 30;
                CurrentPlayer.PowerCost = 15;
                battledata.Text += "Jin Transformed into devil Jin \r\n";
                PowerBar.Value = PowerBar.Value - CurrentPlayer.TransformCost;
                PowerLabel.Text = PowerBar.Value.ToString();
                NewPicture.Image = Properties.Resources.jin2Right;
                if (CurrentPlayer == P1)
                {
                    NewPicture.Image = Properties.Resources.jin2Left;
                }
            }
            if (CurrentPlayer.Name == "Natsu")
            {
                CurrentPlayer.CanUlt = true;
                CurrentPlayer.Upgradable = false;
                CurrentPlayer.Damage = 25;
                battledata.Text += "Natsu unlocked dragonforce \r\n";
                PowerBar.Value = PowerBar.Value - CurrentPlayer.TransformCost;
                PowerLabel.Text = PowerBar.Value.ToString();
                NewPicture.Image = Properties.Resources.natsu2Right;
                if (CurrentPlayer == P1)
                {
                    NewPicture.Image = Properties.Resources.natsu2Left;
                }
            }
            if (CurrentPlayer.Name == "Gray")
            {
                CurrentPlayer.CanUlt = true;
                CurrentPlayer.Upgradable = false;
                CurrentPlayer.Damage = 25;
                battledata.Text += "Gray unlocked devil slayer magic \r\n";
                PowerBar.Value = PowerBar.Value - CurrentPlayer.TransformCost;
                PowerLabel.Text = PowerBar.Value.ToString();
                NewPicture.Image = Properties.Resources.gray2Right;
                if (CurrentPlayer == P1)
                {
                    NewPicture.Image = Properties.Resources.gray2Left;
                }
            }
            CurrentPlayer.Form += 1;
        }
        public void UltimateMethod(Fighters CurrentPlayer, Fighters Opponent, Label PowerLabel, Label HealthLabel, Label MyHealthLabel, ProgressBar PowerBar, ProgressBar Healthbar, ProgressBar Heal, bool Block, PictureBox NewPicture)
        {
            if (CurrentPlayer.PassiveSkip == true)
            {
                if (P1.Name == "Hit")
                {
                    P2Skip = true;
                }
                else
                {
                    P1Skip = true;
                }
            }

            if (Opponent.PassiveDodge == true & rand == 2 & Opponent.Name == "Goku" | Opponent.PassiveDodge == true & rand == 3 & Opponent.Name == "Whis" | Opponent.PassiveDodge == true & rand == 1 & Opponent.Name == "Vegito")
            {
                battledata.Text += Opponent.Name + " has dodged " + CurrentPlayer.Name + "'s ultimate \r\n";
                PowerBar.Value = PowerBar.Value - CurrentPlayer.UltCost;
                PowerLabel.Text = PowerBar.Value.ToString();
            }
            else
            {
                if (CurrentPlayer.PassiveUnblockable)
                {
                    if (Healthbar.Value < CurrentPlayer.Ult)
                    {
                        Healthbar.Value = CurrentPlayer.Ult;
                    }
                    Healthbar.Value = Healthbar.Value - CurrentPlayer.Ult;
                    battledata.Text += CurrentPlayer.Name + " used thier unblockable ultimate dealing " + CurrentPlayer.Ult + " damage \r\n";
                    PowerBar.Value = PowerBar.Value - CurrentPlayer.UltCost;
                    PowerLabel.Text = PowerBar.Value.ToString();
                    HealthLabel.Text = Healthbar.Value.ToString();
                    return;
                }

                if (Block == true)
                {
                    if (Opponent.PassiveHalfDamage == true & rand == 1 & Healthbar.Value < 35)
                    {
                        battledata.Text += Opponent.Name + " took reduced damage from " + CurrentPlayer.Name + "'s ultimate \r\n";

                        PowerBar.Value = PowerBar.Value - CurrentPlayer.UltCost;
                        PowerLabel.Text = PowerBar.Value.ToString();
                        if (Healthbar.Value < CurrentPlayer.Ult / 4)
                        {
                            Healthbar.Value = CurrentPlayer.Ult / 4;
                        }
                        Healthbar.Value = Healthbar.Value - CurrentPlayer.Ult / 4;
                    }


                    if (CurrentPlayer.Name == "Sasuke")
                    {
                        battledata.Text += Opponent.Name + " Has blocked Sasuke's ultmiate and took " + CurrentPlayer.Ult / 2 + " damage. Sasuke gained susanoo \r\n";
                    }
                    else
                    {
                        if (Healthbar.Value < CurrentPlayer.Ult / 2)
                        {
                            Healthbar.Value = CurrentPlayer.Ult / 2;
                        }
                        Healthbar.Value = Healthbar.Value - CurrentPlayer.Ult / 2;
                        battledata.Text += Opponent.Name + " Blocked " + CurrentPlayer.Name + "'s ultmiate and took " + CurrentPlayer.Ult / 2 + " damage \r\n";
                    }
                }
                else
                {
                    if (Opponent.PassiveHalfDamage == true & rand == 1 & Healthbar.Value < 35)
                    {
                        battledata.Text += Opponent.Name + " took reduced damage from " + CurrentPlayer.Name + "'s ultimate \r\n";

                        PowerBar.Value = PowerBar.Value - CurrentPlayer.UltCost;
                        PowerLabel.Text = PowerBar.Value.ToString();
                        if (Healthbar.Value < CurrentPlayer.Ult / 2)
                        {
                            Healthbar.Value = CurrentPlayer.Ult / 2;
                        }
                        Healthbar.Value = Healthbar.Value - CurrentPlayer.Ult / 2;
                    }

                    if (CurrentPlayer.Name == "Sasuke")
                    {
                        battledata.Text += CurrentPlayer.Name + " used his ultmiate dealing " + CurrentPlayer.Ult + " damage and gaining susanoo \r\n";
                    }
                    else
                    {
                        if (Healthbar.Value < CurrentPlayer.Ult)
                        {
                            Healthbar.Value = CurrentPlayer.Ult;
                        }
                        Healthbar.Value = Healthbar.Value - CurrentPlayer.Ult;
                        battledata.Text += CurrentPlayer.Name + " used his ultmiate dealing " + CurrentPlayer.Ult + " damage \r\n";
                    }
                }
            }

            if (CurrentPlayer.Name == "Sasuke")
            {
                if (Opponent.PassiveDodge == true & rand == 2)
                {
                    battledata.Text += "Sasuke gained susanoo \r\n";
                }
                CurrentPlayer.CanUlt = false;
                CurrentPlayer.PassiveChance = 35;
                CurrentPlayer.Damage += 5;
                CurrentPlayer.PowerCost = 15;
                NewPicture.Image = Properties.Resources.sasuke3Left2;
                CurrentPlayer.PassiveChance = 30;
            }

            if (CurrentPlayer.Name == "Vegeta")
            {
                if (Heal.Value < CurrentPlayer.Heal)
                {
                    battledata.Text += CurrentPlayer.Name + " lost " + Heal.Value + " health using his ultimate \r\n";
                    Heal.Value = CurrentPlayer.Heal;
                }
                else
                {
                    battledata.Text += CurrentPlayer.Name + " lost " + CurrentPlayer.Heal + " health using his ultimate \r\n";
                }
                Heal.Value = Heal.Value - CurrentPlayer.Heal;
            }

            if (CurrentPlayer.Name == "Jiren")
            {
                if (Heal.Value >= 100 - CurrentPlayer.Heal)
                {
                    if (Block == true)
                    {
                        if (Heal.Value + CurrentPlayer.Heal / 2 > 100)
                        {
                            Heal.Value = 100 - CurrentPlayer.Heal / 2;
                        }
                        Heal.Value = Heal.Value + CurrentPlayer.Heal / 2;
                        battledata.Text += CurrentPlayer.Name + " gained " + (100 - Heal.Value) + " health using his ultimate \r\n";
                    }
                    else
                    {
                        Heal.Value = 100;
                        battledata.Text += CurrentPlayer.Name + " gained " + (100 - Heal.Value) + " health using his ultimate \r\n";
                    }
                }
                else
                {
                    if (Block == true)
                    {
                        Healthbar.Value = Healthbar.Value - CurrentPlayer.Ult / 2;
                        Heal.Value = Heal.Value + CurrentPlayer.Heal / 2;
                        battledata.Text += CurrentPlayer.Name + " gained " + (100 - Heal.Value) + " health using his ultimate \r\n";
                    }
                    else
                    {
                        Heal.Value = Heal.Value + CurrentPlayer.Heal;
                        battledata.Text += CurrentPlayer.Name + " gained " + CurrentPlayer.Heal + " health using his ultimate \r\n";
                    }
                }
            }
            if (CurrentPlayer.PassiveUnblockable != true)
            {
                if (PowerBar.Value - CurrentPlayer.UltCost < 0)
                {
                    PowerBar.Value = PowerBar.Value + CurrentPlayer.UltCost;
                }
                PowerBar.Value = PowerBar.Value - CurrentPlayer.UltCost;
                PowerLabel.Text = PowerBar.Value.ToString();
                HealthLabel.Text = Healthbar.Value.ToString();
                MyHealthLabel.Text = Heal.Value.ToString();
            }
        }
        public void PowerUpMethod(Fighters CurrentPlayer, ProgressBar PowerBar, Label PowerLabel)
        {
            if (CurrentPlayer.PassiveSteal == true)
            {
                if (PowerBar.Value > 50)
                {
                    PowerBar.Value = 50;
                }
                PowerBar.Value += 50;
                PowerLabel.Text = PowerBar.Value.ToString();
                battledata.Text += CurrentPlayer.Name + " has gained 50 power using his passive steal. \r\n";
                return;
            }
            if (PowerBar.Value > 70)
            {
                PowerBar.Value = 70;
            }
            PowerBar.Value += 30;
            PowerLabel.Text = PowerBar.Value.ToString();
            battledata.Text += CurrentPlayer.Name + " has powered up to " + PowerBar.Value + "% \r\n";
        }
        public void AttackMethod(Fighters CurrentPlayer, Fighters Opponent, bool Block, ProgressBar PowerBar, ProgressBar HealthBar, ProgressBar OpponentPowerBar, Label PowerLabel, Label HealthLabel, Label OpponentPowerLabel)
        {
            if (CurrentPlayer.Name == "Superman" | CurrentPlayer.Name == "Zeno")
            {
                if (HealthBar.Value < CurrentPlayer.Damage)
                {
                    HealthBar.Value = CurrentPlayer.Damage;
                }
                HealthBar.Value = HealthBar.Value - CurrentPlayer.Damage;

                if (Block == true)
                {
                    battledata.Text += CurrentPlayer.Name + "'s attack is unblockable. He dealt " + CurrentPlayer.Damage + " damage \r\n";
                }
                else
                {
                    battledata.Text += CurrentPlayer.Name + " attacked " + Opponent.Name + " dealing " + CurrentPlayer.Damage + " damage \r\n";
                }
                HealthLabel.Text = HealthBar.Value.ToString();
                if (PowerBar.Value - CurrentPlayer.PowerCost < 0)
                {
                    PowerBar.Value = PowerBar.Value + CurrentPlayer.PowerCost;
                }
                PowerBar.Value = PowerBar.Value - CurrentPlayer.PowerCost;
                PowerLabel.Text = PowerBar.Value.ToString();
                return;
            }
            if (Opponent.PassiveDodge == true & Opponent.Name == "Goku")
            {
                battledata.Text += Opponent.Name + " has dodged " + CurrentPlayer.Name + "'s attack using instant transmition \r\n";
                if (PowerBar.Value - CurrentPlayer.PowerCost < 0)
                {
                    PowerBar.Value = PowerBar.Value + CurrentPlayer.PowerCost;
                }
                PowerBar.Value = PowerBar.Value - CurrentPlayer.PowerCost;

                PowerLabel.Text = PowerBar.Value.ToString();
                return;
            }
            if (Opponent.PassiveDodge == true & Opponent.Name != "Goku")
            {
                if (Opponent.Name == "Sasuke" | Opponent.Name == "Madara" | Opponent.Name == "Naruto" | Opponent.Name == "Beerus")
                {
                    if (Opponent.Name == "Naruto")
                    {
                        battledata.Text += CurrentPlayer.Name + "s attack hit " + Opponent.Name + "'s shadow clone \r\n";
                    }
                    if (Opponent.Name == "Sasuke" | Opponent.Name == "Madara")
                    {
                        battledata.Text += Opponent.Name + " has dodged " + CurrentPlayer.Name + "'s attack using his sharingan \r\n";
                    }
                    if (Opponent.Name == "Beerus")
                    {
                        battledata.Text += Opponent.Name + " has dodged " + CurrentPlayer.Name + "'s attack\r\n";
                    }
                    if (PowerBar.Value - CurrentPlayer.PowerCost < 0)
                    {
                        PowerBar.Value = PowerBar.Value + CurrentPlayer.PowerCost;
                    }
                    PowerBar.Value = PowerBar.Value - CurrentPlayer.PowerCost;
                    PowerLabel.Text = PowerBar.Value.ToString();
                }
                else
                {
                    battledata.Text += Opponent.Name + " has dodged " + CurrentPlayer.Name + "'s attack using ultra instinct \r\n";
                    if (PowerBar.Value - CurrentPlayer.PowerCost < 0)
                    {
                        PowerBar.Value = PowerBar.Value + CurrentPlayer.PowerCost;
                    }
                    PowerBar.Value = PowerBar.Value - CurrentPlayer.PowerCost;
                    PowerLabel.Text = PowerBar.Value.ToString();
                }
                return;
            }
            if (Opponent.PassiveAbsorb == true)
            {
                battledata.Text += Opponent.Name + " has absorbed " + CurrentPlayer.Name + "'s attack and gained " + Opponent.Heal + " health \r\n";
                if (HealthBar.Value > 100 - Opponent.Heal)
                {
                    HealthBar.Value = 100 - Opponent.Heal;
                }
                HealthBar.Value = HealthBar.Value + Opponent.Heal;
                HealthLabel.Text = HealthBar.Value.ToString();
                if (PowerBar.Value - CurrentPlayer.PowerCost < 0)
                {
                    PowerBar.Value = PowerBar.Value + CurrentPlayer.PowerCost;
                }
                PowerBar.Value = PowerBar.Value - CurrentPlayer.PowerCost;
                PowerLabel.Text = PowerBar.Value.ToString();
                return;
            }

            if (Opponent.PassiveHalfDamage == true)
            {
                if (Block == true)
                {
                    if (OpponentPowerBar.Value > 90)
                    {
                        OpponentPowerBar.Value = 90;
                    }
                    OpponentPowerBar.Value += 10;
                    OpponentPowerLabel.Text = OpponentPowerBar.Value.ToString();
                    if (Opponent.Name == "Jiren")
                    {
                        battledata.Text += Opponent.Name + " used iron skin while blocking and received 0 damage \r\n";
                    }
                    else
                    {
                        battledata.Text += Opponent.Name + " used their ice make magic while blocking and received 0 damage \r\n";
                    }
                    if (PowerBar.Value - CurrentPlayer.PowerCost < 0)
                    {
                        PowerBar.Value = PowerBar.Value + CurrentPlayer.PowerCost;
                    }
                    PowerBar.Value = PowerBar.Value - CurrentPlayer.PowerCost;
                    PowerLabel.Text = PowerBar.Value.ToString();
                    return;
                }
                else
                {
                    if (PowerBar.Value - CurrentPlayer.PowerCost < 0)
                    {
                        PowerBar.Value = PowerBar.Value + CurrentPlayer.PowerCost;
                    }
                    PowerBar.Value = PowerBar.Value - CurrentPlayer.PowerCost;
                    PowerLabel.Text = PowerBar.Value.ToString();
                    if (Opponent.Name == "Jiren")
                    {
                        battledata.Text += Opponent.Name + " used iron skin and received " + CurrentPlayer.Damage / 2 + " damage \r\n";
                    }
                    else
                    {
                        battledata.Text += Opponent.Name + " used their ice make magic and received " + CurrentPlayer.Damage / 2 + " damage \r\n";
                    }

                    if (HealthBar.Value < CurrentPlayer.Damage / 2)
                    {
                        HealthBar.Value = CurrentPlayer.Damage / 2;
                    }
                    HealthBar.Value = HealthBar.Value - CurrentPlayer.Damage / 2;
                    HealthLabel.Text = HealthBar.Value.ToString();
                    return;
                }
            }

            if (CurrentPlayer.PassiveDoubleDamage == true)
            {
                if (Block == true)
                {
                    if (OpponentPowerBar.Value > 90)
                    {
                        OpponentPowerBar.Value = 90;
                    }
                    OpponentPowerBar.Value += 10;
                    OpponentPowerLabel.Text = OpponentPowerBar.Value.ToString();
                    if (HealthBar.Value < CurrentPlayer.Damage)
                    {
                        HealthBar.Value = CurrentPlayer.Damage;
                    }
                    if (CurrentPlayer.Name == "Vegeta")
                    {
                        battledata.Text += Opponent.Name + " has blocked " + CurrentPlayer.Name + "'s rage and received " + CurrentPlayer.Damage + " damage \r\n";
                    }
                    else
                    {
                        battledata.Text += Opponent.Name + " has blocked " + CurrentPlayer.Name + "'s assault and received " + CurrentPlayer.Damage + " damage \r\n";
                    }
                    HealthBar.Value = HealthBar.Value - CurrentPlayer.Damage;
                    HealthLabel.Text = HealthBar.Value.ToString();
                    if (PowerBar.Value - CurrentPlayer.PowerCost < 0)
                    {
                        PowerBar.Value = PowerBar.Value + CurrentPlayer.PowerCost;
                    }
                    PowerBar.Value = PowerBar.Value - CurrentPlayer.PowerCost;
                    PowerLabel.Text = PowerBar.Value.ToString();
                    return;
                }
                else
                {
                    if (CurrentPlayer.Name == "Vegeta")
                    {
                        battledata.Text += CurrentPlayer.Name + " has used rage dealing " + CurrentPlayer.Damage * 2 + " damage \r\n";
                    }
                    else
                    {
                        battledata.Text += CurrentPlayer.Name + " landed a critical hit dealing " + CurrentPlayer.Damage * 2 + " damage \r\n";
                    }
                    if (HealthBar.Value < CurrentPlayer.Damage * 2)
                    {
                        if (CurrentPlayer.Damage * 2 > 100)
                        {
                            HealthBar.Value = 100;
                        }
                        else
                        {
                            HealthBar.Value = CurrentPlayer.Damage * 2;
                        }
                    }
                    HealthBar.Value = HealthBar.Value - CurrentPlayer.Damage * 2;
                    HealthLabel.Text = HealthBar.Value.ToString();
                    if (PowerBar.Value - CurrentPlayer.PowerCost < 0)
                    {
                        PowerBar.Value = PowerBar.Value + CurrentPlayer.PowerCost;
                    }
                    PowerBar.Value = PowerBar.Value - CurrentPlayer.PowerCost;
                    PowerLabel.Text = PowerBar.Value.ToString();
                    return;
                }
            }
            if (Block == true)
            {
                if (OpponentPowerBar.Value > 90)
                {
                    OpponentPowerBar.Value = 90;
                }
                if (HealthBar.Value < CurrentPlayer.Damage / 2)
                {
                    HealthBar.Value = CurrentPlayer.Damage / 2;
                }
                OpponentPowerBar.Value += 10;
                OpponentPowerLabel.Text = OpponentPowerBar.Value.ToString();
                HealthBar.Value = HealthBar.Value - CurrentPlayer.Damage / 2;
                battledata.Text += Opponent.Name + " has blocked " + CurrentPlayer.Name + "'s normal attack and took " + CurrentPlayer.Damage / 2 + " damage \r\n";
            }
            else
            {
                if (HealthBar.Value < CurrentPlayer.Damage)
                {
                    HealthBar.Value = CurrentPlayer.Damage;
                }
                HealthBar.Value = HealthBar.Value - CurrentPlayer.Damage;
                battledata.Text += CurrentPlayer.Name + " has successfully attacked " + Opponent.Name + " dealing " + CurrentPlayer.Damage + " damage \r\n";
            }
            HealthLabel.Text = HealthBar.Value.ToString();
            if (PowerBar.Value - CurrentPlayer.PowerCost < 0)
            {
                PowerBar.Value = PowerBar.Value + CurrentPlayer.PowerCost;
            }
            PowerBar.Value = PowerBar.Value - CurrentPlayer.PowerCost;
            PowerLabel.Text = PowerBar.Value.ToString();
        }
        public void PassiveMethod(Fighters CurrentPlayer, ProgressBar CurrentPlayerPowerBar, ProgressBar CurrentPlayerHealthBar, Label CurrentPlayerPowerLabel, Label CurrentPlayerHealthLabel)
        {
            ran2 = rnd.Next(1, 100);

            if (CurrentPlayer.Name == "Goku" & ran2 >= 1 & ran2 <= CurrentPlayer.PassiveChance)
            {
                if (CurrentPlayerPowerBar.Value > 80)
                {
                    CurrentPlayerPowerBar.Value = 80;
                }
                battledata.Text += CurrentPlayer.Name + " gained 20 energy from his passive \r\n";
                CurrentPlayerPowerBar.Value += 20;
                CurrentPlayerPowerLabel.Text = CurrentPlayerPowerBar.Value.ToString();
            }
            if (CurrentPlayer.Name == "Vegeta" & ran2 >= 1 & ran2 <= CurrentPlayer.PassiveChance)
            {
                if (CurrentPlayerHealthBar.Value > 75)
                {
                    CurrentPlayerHealthBar.Value = 75;
                }
                if (CurrentPlayerHealthBar.Value <= 40)
                {
                    CurrentPlayer.PassiveChance = 40;
                }
                battledata.Text += CurrentPlayer.Name + " gained 25 health from his passive \r\n";
                CurrentPlayerHealthBar.Value += 25;
                CurrentPlayerHealthLabel.Text = CurrentPlayerHealthBar.Value.ToString();
            }
            if (CurrentPlayer.Name == "Gohan" & CurrentPlayerHealthBar.Value < 50 & CurrentPlayer.Form == 3)
            {
                CurrentPlayer.Damage = 40;
                if (CurrentPlayerPowerBar.Value >= 90)
                {
                    CurrentPlayerPowerBar.Value = 90;
                }
                CurrentPlayerPowerBar.Value += 10;
                CurrentPlayerPowerLabel.Text = CurrentPlayerPowerBar.Value.ToString();
                battledata.Text += "Gohan gained 10 energy from his passive \r\n";
            }
            if (CurrentPlayer.Name == "Jiren" | CurrentPlayer.Name == "Toppo" & CurrentPlayerHealthBar.Value <= 40)
            {
                CurrentPlayer.PassiveChance = 70;
                CurrentPlayer.Ult = 80;
            }
            if (CurrentPlayer.Name == "Jiren" | CurrentPlayer.Name == "Gray" & ran2 >= 1 & ran2 <= CurrentPlayer.PassiveChance)
            {
                CurrentPlayer.PassiveHalfDamage = true;
            }
            if (CurrentPlayer.Name == "Vegito" | CurrentPlayer.Name == "Whis" | CurrentPlayer.Name == "Sasuke" | CurrentPlayer.Name == "Madara" | CurrentPlayer.Name == "Goku" | CurrentPlayer.Name == "Naruto" & ran2 >= 1 & ran2 <= CurrentPlayer.PassiveChance)
            {
                CurrentPlayer.PassiveDodge = true;
            }
            if (CurrentPlayer.Name == "Frieza" & ran2 >= 1 & ran2 <= CurrentPlayer.PassiveChance)
            {
                CurrentPlayer.PassiveSteal = true;
            }
            if (CurrentPlayer.Name == "Android 17" & ran2 >= 1 & ran2 <= CurrentPlayer.PassiveChance)
            {
                CurrentPlayer.PassiveAbsorb = true;
            }
            if (CurrentPlayer.Name == "Hit" & ran2 >= 1 & ran2 <= CurrentPlayer.PassiveChance)
            {
                CurrentPlayer.PassiveSkip = true;
            }
            if (CurrentPlayer.Name == "Beerus" | CurrentPlayer.Name == "Vegeta" | CurrentPlayer.Name == "Natsu" & ran2 >= 1 & ran2 <= CurrentPlayer.PassiveChance)
            {
                CurrentPlayer.PassiveDoubleDamage = true;
            }
        }
        private void button8_Click(object sender, EventArgs e)
        {
            pause();

        }
        private void button7_Click(object sender, EventArgs e)
        {
            pause();
        }
        public void RestartMethod(Fighters CurrentPlayer, ProgressBar CurrentPlayerPowerBar, ProgressBar CurrentPlayerHealthBar, Label CurrentPlayerPowerLabel, Label CurrentPlayerHealthLabel)
        {
            CurrentPlayerHealthBar.Value = 100;
            CurrentPlayerHealthLabel.Text = "100";
            CurrentPlayerPowerBar.Value = 70;
            CurrentPlayerPowerLabel.Text = "70";
            CurrentPlayer.PassiveUnblockable = false;
            CurrentPlayer.Form = 0;
            CurrentPlayer.CanUlt = false;
            CurrentPlayer.Damage = 20;
            CurrentPlayer.PowerCost = 20;
            if (CurrentPlayer.Name == "Zeno")
            {
                CurrentPlayer.Damage = 50;
                CurrentPlayerPowerBar.Value = 100;
                CurrentPlayerPowerLabel.Text = "100";
                CurrentPlayer.PowerCost = 0;
            }
            if (CurrentPlayer.Name == "Madara")
            {
                CurrentPlayer.UltCost = 60;
                CurrentPlayer.Damage = 25;
                CurrentPlayer.TransformCost = 70;
            }
            if (CurrentPlayer.Name == "Hit")
            {
                CurrentPlayer.Ult = 70;
            }
            if (CurrentPlayer.Name == "Hit" | CurrentPlayer.Name == "Frieza")
            {
                CurrentPlayer.PassiveChance = 35;
            }
            if (CurrentPlayer.Name == "Frieza" | CurrentPlayer.Name == "Madara")
            {
                CurrentPlayer.Ult = 50;
            }
            if (CurrentPlayer.Name == "Beerus" | CurrentPlayer.Name == "Android 17")
            {
                CurrentPlayer.PowerCost = 15;
            }
            if (CurrentPlayer.Name == "Android 17" | CurrentPlayer.Name == "Gohan")
            {
                CurrentPlayer.Damage = 15;
            }
            if (CurrentPlayer.Name == "Goku" | CurrentPlayer.Name == "Toppo" | CurrentPlayer.Name == "Android 17" | CurrentPlayer.Name == "Gohan" | CurrentPlayer.Name == "Sasuke")
            {
                CurrentPlayer.PassiveChance = 15;
            }
            if (CurrentPlayer.Name == "Vegito" | CurrentPlayer.Name == "Jiren")
            {
                CurrentPlayer.PassiveChance = 25;
            }
            if (CurrentPlayer.Name == "Vegeta")
            {
                CurrentPlayer.PassiveChance = 10;
            }
            if (CurrentPlayer.Name == "Hit" | CurrentPlayer.Name == "Jiren" | CurrentPlayer.Name == "Beerus")
            {
                CurrentPlayer.Damage = 30;
            }
            if (CurrentPlayer.Name == "Whis" | CurrentPlayer.Name == "Saitama")
            {
                CurrentPlayer.Damage = 25;
            }
            if (CurrentPlayer.Name == "Whis")
            {
                CurrentPlayer.PassiveChance = 50;
            }
            if (CurrentPlayer.Name == "Natsu" | CurrentPlayer.Name == "Gray" | CurrentPlayer.Name == "Madara" | CurrentPlayer.Name == "Naruto" | CurrentPlayer.Name == "Beerus")
            {
                CurrentPlayer.PassiveChance = 20;
            }
            if (CurrentPlayer.Name == "Vegeta" | CurrentPlayer.Name == "Jiren" | CurrentPlayer.Name == "Saitama" | CurrentPlayer.Name == "Superman" | CurrentPlayer.Name == "Zeno")
            {
                CurrentPlayer.CanUlt = true;
            }
            if (CurrentPlayer.TransformCost > 5)
            {
                CurrentPlayer.Upgradable = true;
            }
        }
        public bool AI(Fighters CurrentPlayer, Fighters Opponent, ProgressBar MyHP, ProgressBar NmeHP, ProgressBar MyPower, ProgressBar NmePower, ref bool attack, ref bool block, ref bool powerup, ref bool transform, ref bool ult)
        {
            int AI = 0;
            AI = AIrandom.Next(1, 3);

            if (Opponent.Name == "Whis" & Opponent.Form == 0 | Opponent.Name == "Vegito" & Opponent.Form == 0 & MyPower.Value >= CurrentPlayer.PowerCost)
            {
                attack = true;
                LockIn();
                return attack;
            }

            // i can kill | i have energy to kill | opponent can ult | opponent has enough power | i can survive opponents ult if i block
            if (CurrentPlayer.Damage >= NmeHP.Value & MyPower.Value >= CurrentPlayer.PowerCost - 10 & Opponent.CanUlt == true & NmePower.Value >= Opponent.UltCost & MyHP.Value > Opponent.Ult / 2)
            {
                block = true;
                LockIn();
                return block;
            }

            // i can kill | he can kill | he cant kill if i block | he can attack | he can't attack twice
            if (AI == 1 & CurrentPlayer.Damage >= NmeHP.Value & MyPower.Value >= CurrentPlayer.PowerCost - 10 & MyHP.Value < Opponent.Damage & MyHP.Value > Opponent.Damage / 2 & 
                NmePower.Value >= Opponent.PowerCost & NmePower.Value < Opponent.PowerCost * 2)
            {
                block = true;
                LockIn();
                return block;
            }
            if (AI == 2 & CurrentPlayer.Damage >= NmeHP.Value & MyPower.Value >= CurrentPlayer.PowerCost - 10 & MyHP.Value < Opponent.Damage & MyHP.Value > Opponent.Damage / 2 & 
                NmePower.Value >= Opponent.PowerCost & NmePower.Value < Opponent.PowerCost * 2)
            {
                powerup = true;
                LockIn();
                return powerup;
            }
            if (AI == 3 & CurrentPlayer.Damage >= NmeHP.Value & MyPower.Value >= CurrentPlayer.PowerCost - 10 & MyHP.Value < Opponent.Damage & MyHP.Value > Opponent.Damage / 2 & 
                NmePower.Value >= Opponent.PowerCost & NmePower.Value < Opponent.PowerCost * 2)
            {
                attack = true;
                LockIn();
                return attack;
            }

            // i can ult | ult will kill | enough power to ult | he can kill | he cant kill if i block | he can attack | he cant attack twice
            if (CurrentPlayer.CanUlt == true & CurrentPlayer.Ult >= NmeHP.Value & MyPower.Value >= CurrentPlayer.UltCost - 10 & MyHP.Value < Opponent.Damage & 
                MyHP.Value > Opponent.Damage / 2 & NmePower.Value >= Opponent.PowerCost & NmePower.Value < Opponent.PowerCost * 2)
            {
                block = true;
                LockIn();
                return block;
            }

            if (CurrentPlayer.CanUlt == true & MyPower.Value >= CurrentPlayer.UltCost & roundNumber >= 5) // if you can ult, ult starting at round 5
            {
                ult = true;
                LockIn();
                return ult;
            }

            if (NmeHP.Value <= CurrentPlayer.Damage & MyPower.Value >= CurrentPlayer.PowerCost) // if you can kill, kill
            {
                attack = true;
                LockIn();
                return attack;
            }

            if (Opponent.Damage == MyHP.Value & Opponent.PowerCost <= NmePower.Value) //if opponents attack will kill, block
            {
                block = true;
                LockIn();
                return block;
            }

            if (Opponent.CanUlt == true & NmePower.Value >= Opponent.UltCost) //if opponent can ult, block
            {
                if (Opponent.Ult / 2 >= MyHP.Value & CurrentPlayer.CanUlt == true & MyPower.Value >= CurrentPlayer.UltCost & roundNumber >= 5) //if opponent can kill use your ult if possible
                {
                    ult = true;
                    LockIn();
                    return ult;
                }
                if (Opponent.Ult / 2 >= MyHP.Value & CurrentPlayer.CanUlt == false & MyPower.Value >= CurrentPlayer.PowerCost) //if cant ult and opponent can kill you through block, attack
                {
                    attack = true;
                    LockIn();
                    return attack;
                }
                block = true;
                LockIn();
                return block;
            }

            if (CurrentPlayer.CanUlt == true & MyHP.Value <= Opponent.Damage & MyPower.Value >= CurrentPlayer.UltCost - 30 & MyPower.Value < 100) //if can ult but not enough energy and opponent can't kill, power up
            {
                powerup = true;
                LockIn();
                return powerup;
            }

            if (Opponent.Upgradable == true & NmePower.Value >= Opponent.TransformCost) // if opponent can transform
            {
                if (CurrentPlayer.CanUlt == true & MyPower.Value >= CurrentPlayer.UltCost & roundNumber >= 5) // if you can ult while they can transform, ult
                {
                    ult = true;
                    LockIn();
                    return ult;
                }
                if (CurrentPlayer.CanUlt == true & MyPower.Value >= CurrentPlayer.UltCost - 60 & MyPower.Value < 100) // if you can ult in the next 2 rounds while they can transform, powerup
                {
                    powerup = true;
                    LockIn();
                    return powerup;
                }
                if (CurrentPlayer.PowerCost >= MyPower.Value & MyPower.Value >= CurrentPlayer.PowerCost) // if you can attack while they transform, attack
                {
                    attack = true;
                    LockIn();
                    return attack;
                }
            }

            if (CurrentPlayer.PassiveUnblockable == true & MyPower.Value > CurrentPlayer.UltCost & CurrentPlayer.CanUlt == true & MyPower.Value < 100) // if unblockable ult, power up
            {
                powerup = true;
                LockIn();
                return powerup;
            }

            if (MyPower.Value < CurrentPlayer.PowerCost) // if you have no energy, power up
            {
                powerup = true;
                LockIn();
                return powerup;
            }

            if (Opponent.PassiveDodge == true & MyPower.Value < 100)
            {
                powerup = true;
                LockIn();
                return powerup;
            }

            if (Opponent.Name == "Vegito" | Opponent.Name == "Whis" & MyPower.Value >= CurrentPlayer.PowerCost)
            {
                attack = true;
                LockIn();
                return attack;
            }

            if (CurrentPlayer.Upgradable == true & MyPower.Value >= CurrentPlayer.TransformCost) // if you can transform, transform
            {
                transform = true;
                LockIn();
                return transform;
            }

            if (CurrentPlayer.Upgradable == true & MyPower.Value >= CurrentPlayer.TransformCost - 30 & Opponent.Damage < MyHP.Value & MyPower.Value < 100) // if can transform next round and wont die this round, power up
            {
                powerup = true;
                LockIn();
                return powerup;
            }

            if (ran == 1 & MyPower.Value >= CurrentPlayer.PowerCost)
            {
                attack = true;
                LockIn();
                return attack;
            }
            if (ran == 2)
            {
                block = true;
                LockIn();
                return block;
            }
            if (ran == 3 & MyPower.Value < 100)
            {
                powerup = true;
                LockIn();
                return powerup;
            }
            return false;

        }
        private void pause()
        {
            if (timer1.Enabled == true)
            {
                timer1.Enabled = false;
                timer2.Enabled = false;
                timer3.Enabled = false;
                timer4.Enabled = false;
            }
            else
            {
                timer1.Enabled = true;
                timer2.Enabled = true;
                timer3.Enabled = true;
                timer4.Enabled = true;
            }
        }
        private void wincheck() //check if player wins
        {
            if (p1HealthBar.Value == 0 && p2HealthBar.Value == 0) //IF P1 AND P2 DIE
            {
                timer1.Stop();
                timer2.Stop();
                timer3.Stop();
                timer4.Stop();

                if (Player2RB2.Checked == true & Player1RB2.Checked == true)
                {
                    Player1RB1.Checked = true;
                    Player2RB1.Checked = true;
                    MessageBox.Show("GAME OVER: DRAW");
                    using (Form2 form2 = new Form2())
                    {
                        if (form2.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                        {
                        }
                    }
                    return;
                }

                if (Player2RB3.Checked == true)
                {
                    if (Player2RB2.Checked == true)
                    {
                        Player2RB1.Checked = true;
                        using (Form2 form2 = new Form2())
                        {
                            if (form2.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                            {
                            }
                        }
                        return;
                    }
                    Player2RB2.Checked = true;
                }
                else
                {
                    Player2RB3.Checked = true;
                }

                if (Player1RB3.Checked == true)
                {
                    if (Player1RB2.Checked == true)
                    {
                        Player1RB1.Checked = true;
                        using (Form2 form2 = new Form2())
                        {
                            if (form2.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                            {
                            }
                        }
                        return;
                    }
                    Player1RB2.Checked = true;
                }
                else
                {
                    Player1RB3.Checked = true;
                }
                MessageBox.Show("Double KO");
                battledata.Text += "Battle ended in a double KO" + "\r\n";
                battledata.Text += "_______________________________________\r\n";

                RoundCount++;
                if (Player1RB1.Checked == false | Player2RB1.Checked == false)
                {
                    RestartMethod(P1, p1PowerBar, p1HealthBar, p1powerbarLabel, p1healthbarLabel);
                    RestartMethod(P2, p2PowerBar, p2HealthBar, p2powerbarLabel, p2healthbarLabel);
                    Battleground_Form_Load(this, null);
                }
                return;
            }
            if (p2HealthBar.Value == 0) // IF P2 DIES
            {
                timer1.Stop();
                timer2.Stop();
                timer3.Stop();
                timer4.Stop();
                if (Player1RB3.Checked == true)
                {
                    if (Player1RB2.Checked == true)
                    {
                        Player1RB1.Checked = true;
                        MessageBox.Show("GAME OVER: " + P1.Name + " Wins");
                        using (Form2 form2 = new Form2())
                        {
                            if (form2.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                            {
                            }
                        }
                        return;
                    }
                    Player1RB2.Checked = true;
                }
                Player1RB3.Checked = true;
                MessageBox.Show(P1.Name + " has won in " + roundNumber + " rounds");
                battledata.Text += P1.Name + " won the battle in round " + roundNumber + "\r\n";
                battledata.Text += "_______________________________________\r\n";
                RoundCount++;
                if (Player1RB1.Checked == false | Player2RB1.Checked == false)
                {
                    RestartMethod(P1, p1PowerBar, p1HealthBar, p1powerbarLabel, p1healthbarLabel);
                    RestartMethod(P2, p2PowerBar, p2HealthBar, p2powerbarLabel, p2healthbarLabel);
                    Battleground_Form_Load(this, null);
                }
            }
            if (p1HealthBar.Value == 0) //IF P1 DIES
            {
                timer1.Stop();
                timer2.Stop();
                timer3.Stop();
                timer4.Stop();

                if (Player2RB3.Checked == true)
                {
                    if (Player2RB2.Checked == true)
                    {
                        Player2RB1.Checked = true;
                        MessageBox.Show("GAME OVER: " + P2.Name + " Wins");
                        using (Form2 form2 = new Form2())
                        {
                            if (form2.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                            {
                                this.Close();
                            }
                        }
                        return;
                    }
                    Player2RB2.Checked = true;
                }
                Player2RB3.Checked = true;
                MessageBox.Show(P2.Name + " has won in " + roundNumber + " rounds");
                battledata.Text += P2.Name + " won the battle in round " + roundNumber + "\r\n";
                battledata.Text += "_______________________________________\r\n";
                RoundCount++;
                if (Player1RB1.Checked == false | Player2RB1.Checked == false)
                {
                    RestartMethod(P1, p1PowerBar, p1HealthBar, p1powerbarLabel, p1healthbarLabel);
                    RestartMethod(P2, p2PowerBar, p2HealthBar, p2powerbarLabel, p2healthbarLabel);
                    Battleground_Form_Load(this, null);
                }
            }      
            if (Player1RB1.Checked == true)
            {
                timer1.Stop();
                timer2.Stop();
                timer3.Stop();
                timer4.Stop();
                MessageBox.Show("GAME OVER: " + P1.Name + " Wins");
                using (Form2 form2 = new Form2())
                {
                    if (form2.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        this.Close();
                    }
                }
            }
            if (Player2RB1.Checked == true)
            {
                timer1.Stop();
                timer2.Stop();
                timer3.Stop();
                timer4.Stop();
                MessageBox.Show("GAME OVER: " + P2.Name + " Wins");
                using (Form2 form2 = new Form2())
                {
                    if (form2.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        this.Close();
                    }
                }
            }
        }
        private void CurrentCharacterStats(Fighters CurrentPlayer, TextBox lb) //display stats
        {
            lb.Text = "Name: " + CurrentPlayer.Name + "\r\nDamage: " + CurrentPlayer.Damage + "\r\nAttack cost: " + CurrentPlayer.PowerCost +
                "\r\nTransform cost: " + CurrentPlayer.TransformCost + "\r\nUlt: " + CurrentPlayer.Ult + "\r\nUlt cost: " + CurrentPlayer.UltCost + "\r\nPassive chance: " + CurrentPlayer.PassiveChance;
        }
    }
}